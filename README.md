## Overview
This project is a one-year long game project using Unity. 

## Coding Style
- Class name using **UpperCamelCase**. 
- Function name using **UpperCamelCase**.
- Constant using **SNAKE_CASE**
- Variable name using **camelCase**

## Folder structure
- **Editor** put editor script/custom inspector in this folder.
- **EditorGUI** Additional mono behavior scipts 
- **Materials** Materials for enviroment, terrain, water, etc.
- **meshes** 3D Models imported from blender. Keep each model in a separate folder.
- **Prefabs** Prefabs
- **Scenes** Scenes
- **scripts** GamePlay related scripts
- **textures** Environment related textures.

## Event System

(Yiou) I added a event system based on [this tutorial](https://unity3d.com/learn/tutorials/topics/scripting/events-creating-simple-messaging-system). The event system can facilitate communication between components, so that we don't need to link them together. This will greatly simplify the process of creating a new stage and will reduce error.

The main class of event system is `EventManagerScript.cs`. I created a prefab call `EventManager`. This should be added to a scene.  

The `EventManagerScript` is a singleton that contains mainly three functions. 

- `AddEventListener(string eventName, UnityAction<BaseEvent> listener))`: Add an event listener for an event, when the event is fired, run listener. I will talk about listener in detail later. 
- `RemoveEventListener(string eventName, UnityAction<BaseEvent> listener)`: Remove an event listener for an event
- `FireEvent(string eventName, BaseEvent eventObj)`: Fire a event with eventname, and event obj. I will talk about the eventObj later.


In order to better demonstrate the usage of Event System, see the following example.  

We want to display the boss's hp bar when player reaches the boss stage. What I did was: 
1. Every time player arrives at a waypoint, the `PlayerMovementScript` calls `FireEvent` to fire a `ArriveWaypointEvent`, which extends the `BaseEvent`.
2. `HUDManager` `AddEventListener` to listener for `ArriveWaypointEvent`, and gives a callback listener.
3. When the `ArriveWaypointEvent` is fired, the callback listener checks the information of this `ArriveWaypointEvent` to see which waypoint. If the last one, turn on the HP bar opacity.

### listener
Listener, or callback is defined by the parties that are interested in the event. They want some functions to be called when the event fires. This is implemented using C# delegate. [Delegate](https://msdn.microsoft.com/en-us/library/ms173171.aspx) is a very powerful concept in C# for functional programming. It defines the types of a function (argument type, return type). So you can save a function in a variable as this delegate type. `UnityAction<BaseEvent>` is a function type that takes on `BaseEvent` argument, and returns void. What's powerful is, you can store a class method as this delegate type, and pass it along. 

### BaseEvent
BaseEvent is for event firer to broadcast additional information at current state. We can extends that to create different event classes that suit different need. I already created some Event classes, and fire them at some point. But I haven't refactored the code to consume them.

Events are in `Events` folder:

- `ArriveWaypointEvent`: Fired when player reaches a waypoint.
- `BossDieEvent`: Fired when boss die.
- `BossHitEvent`: Fired when boss got hit.
- `BossHPChangeEvent`: Fired when boss's hp change. (HUD listen to that to update the HP bar. The reason why I use this event instead fo `BossHitEvent`, is because I was thinking about situation when boss recovers its HP, then `BossHitEvent` doesn't make sense)
- `EnemyDieEvent`: Fired when enemy die. (We can use this to update the score, or collect rewards)
- More to be added


## Some other notable changes

Added `waypointID` attribute to `waypointScript`. This is so that when an `ArriveWaypoint` fires, listener can know which waypoint. Most of the waypoints don't need to set this attribute. Only the important ones, such as the boss waypoint.

## How to simplify terrain

This took me so long to find out. In order to simplify a terrain from unity, follow the following steps:

1. export terrain from unity from menu -> Terrain -> Export to obj
2. Select ratio "half"
3. import the exported obj into blender
4. Select the terrain in object mode. change mode to sculpt mode
5. In sculpt mode, in the bottom of the 3D view, choose brush, select `simplify`
6. On the panel on the right of 3D view (brush settings). toggle `dyntopo`, short for dynamic topology.
7. change the detail level of dyntopo. zoom in and zoom out while brush over the terrain. The further you zoom out, the more aggressive the simplification will be.