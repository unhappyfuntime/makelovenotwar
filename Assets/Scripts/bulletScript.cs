﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour {

    public bool lethal;
    [HideInInspector]
    public Vector3 direction;

    private Vector3 prevPos;

    IEnumerator disappear()
    {
        yield return new WaitForSeconds(2f);
        RemoveSelf();
    }
    IEnumerator hitPlayer()
    {
        AudioManagerScript.Instance.RequestFX(7, 0.6f, GameManagerScript.Instance.Player.transform);
        yield return new WaitForSeconds(0.6f);
        if (PlayerMovementScript.Instance.isInAction && !GameManagerScript.Instance.invincible)
            GameManagerScript.Instance.getHit();
        RemoveSelf();
    }

    void RemoveSelf()
    {
        if (this.lethal)
        {
            EventManagerScript.FireEvent(AccurateBulletGoneEvent.NAME, new AccurateBulletFireEvent());
        }
        Destroy(gameObject);
    }
	// Use this for initialization
	void Start () {
        transform.rotation = Quaternion.LookRotation(direction);
        if (lethal)
        {
            EventManagerScript.FireEvent(AccurateBulletFireEvent.NAME, new AccurateBulletFireEvent());
            StartCoroutine(hitPlayer());
        } else
        {
            StartCoroutine(disappear());
        }
    }
	
	// Bullet flies away until it dies
	void FixedUpdate () {
        transform.position = transform.position + direction * Time.deltaTime * 25;
	}
}
