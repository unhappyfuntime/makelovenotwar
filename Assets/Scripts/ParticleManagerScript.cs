﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManagerScript : Singleton<ParticleManagerScript> {
    public Transform[] prefabs;
	
    public void requestParticlePrefab(int index, Vector3 location)
    {
        var prefab = Instantiate(prefabs[index]) as Transform;
        prefab.SetParent(this.transform);
        prefab.position = location;
    }
    public void requestParticlePrefab(int index, Vector3 location, Transform parent)
    {
        var prefab = Instantiate(prefabs[index]) as Transform;
        prefab.SetParent(parent);
        prefab.position = location;
    }
}
