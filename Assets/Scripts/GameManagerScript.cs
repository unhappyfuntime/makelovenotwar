﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public enum GameState
{
    RUNNING, PAUSE, STOP
}

public enum TimerState
{
    RUNNING, PAUSE, STOP
}

/**
 * GameManagerScript handles the most essential state information. 
 * It does nothing other then storing the state. 
 * Any modification of and access to the data should be initiated from outside.
 */
public class GameManagerScript : Singleton<GameManagerScript> {

    public int score;
    
    public int health;
    [HideInInspector]
    public float timePlayed;
    public float timeLeft;
    [HideInInspector]
    public PlayerAmmoScript ammo;
    [HideInInspector]
    public PlayerMovementScript movement;
    [HideInInspector]
    public GameObject HUD;
    public GameObject Player;
    [HideInInspector]
    public bool invincible = false;
    public bool bossDead = false;
    
    public int initHealth = 4;
    public float initTime = 30.0f;

    private GameState gameState = GameState.STOP;
    private TimerState timerState = TimerState.RUNNING;

    public static bool gameRunning
    {
        get
        {
            return GameManagerScript.Instance.gameState == GameState.RUNNING;
        }
    }

    public static bool timerRunning
    {
        get
        {
            return GameManagerScript.Instance.timerState == TimerState.RUNNING;
        }
    }

    public void getHit()
    {
        health--;
        HUDManagerScript.Instance.getHit();
        CameraShakeScript.Instance.shake();
        AudioManagerScript.Instance.RequestFX(4, Player.transform);
        invincible = true;
        StartCoroutine(makeInvincible());
    }
    IEnumerator makeInvincible()
    {
        yield return new WaitForSeconds(0.6f);
        invincible = false;
    }

    public void InitGame()
    {
        health = initHealth;
        timeLeft = initTime;
        timePlayed = 0f;
        //Debug.Log("initing gun");
        ammo.Init();
    }

    public void OnEnemyDie(BaseEvent e) {
        //EnemyDieEvent _event = e as EnemyDieEvent;
        //Debug.Log(_event.name + _event.enemyType);
    }

    public void OnBossDie(BaseEvent e)
    {
        //Debug.Log("boss die, fire game over event");
        EventManagerScript.FireEvent(GameOverEvent.NAME, new GameOverEvent(GameOutcome.WIN, this.score));
    }

    public void OnGameOver(BaseEvent e)
    {
        GameOverEvent _event = e as GameOverEvent;
        PersistentFileScript.data.TryUpdateHighScore(SceneManager.GetActiveScene().name, _event.score);
        PersistentFileScript.Save();
        // clean up.
        StopGame();
        //HUD.SetActive(false);
    }
    public void StartGame()
    {
        InitGame();
        gameState = GameState.RUNNING;
        timerState = TimerState.RUNNING;
        //EventManagerScript.AddEventListener(EnemyDieEvent.NAME, this.OnEnemyDie);
        EventManagerScript.AddEventListener(BossDieEvent.NAME, this.OnBossDie);
        EventManagerScript.AddEventListener(GameOverEvent.NAME, this.OnGameOver);
    }

    void Start()
    {
        ammo = PlayerAmmoScript.Instance;
        movement = PlayerMovementScript.Instance;
        //for debugging purpose, start right away
        StartGame();
    }
    // Update is called once per frame
    void Update () {
        UpdateGame();
        UpdateTimer();
	}

    void StopGame()
    {
        gameState = GameState.STOP;
        timerState = TimerState.STOP;
    }

    void PauseGame()
    {
        if (gameState == GameState.STOP)
        {
            Debug.LogError("Cannot pause game when game is not stopped");
        } else
        {
            gameState = GameState.PAUSE;
            PauseTimer();
        }
    }

    public void PauseTimer()
    {
        if (timerState == TimerState.STOP)
        {
            Debug.LogError("Cannot pause timer when timer is stopped");
        } else
        {
            timerState = TimerState.PAUSE;
        }
    }

    void ResumeGame()
    {
        if (gameState == GameState.STOP)
        {
            Debug.LogError("Cannot resume game when game is stopped");
        } else
        {
            gameState = GameState.RUNNING;
            ResumeTimer();
        }
    }

    public void ResumeTimer()
    {
        if (timerState == TimerState.STOP)
        {
            Debug.LogError("Cannot resume timer when timer is stopped");
        }
        else
        {
            timerState = TimerState.RUNNING;
        }
    }
    
    public bool TimerRunning()
    {
        return timerState == TimerState.RUNNING;
    }

    void UpdateGame()
    {
        if (gameState == GameState.RUNNING)
        {
            if (movement.currentWaypoint.CheckTag(MovementStates.HOLDING) 
                && movement.movementState == MovementStates.HOLDING
                && movement.currentWaypoint.remainingEnemies <= 0)
            {
                //finish all the enemy
                if (!movement.Proceed() && bossDead)
                {
                    StopGame();
                }
            }
        }
    }
    void UpdateTimer()
    {
        if (timeLeft <= 0)
        {
            //StopGame();
        }
        if (timerState == TimerState.RUNNING)
        {
            timeLeft -= Time.deltaTime;
            timePlayed += Time.deltaTime;
            if (timeLeft < 0)
                timeLeft = 0;
        }
    }
}
