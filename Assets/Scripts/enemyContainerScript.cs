﻿using UnityEngine;
using System.Collections;

public class enemyContainerScript : Singleton<enemyContainerScript> {

    public Transform[] enemyTypes;

   

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void KillAllEnemies()
    {
        foreach (Transform enemy in transform)
        {
            enemy.GetComponent<enemyScript>().Die();
        }
    }
}
