﻿using UnityEngine;
using System.Collections;

public class WaypointScript : MonoBehaviour {

    
    public float turnSpeed = 5;
    public float moveSpeedFraction = 1.0f / 50.0f;

    public Vector3 hidingPos; // relative translation the camera will move when hiding.
    public enemySpawnerScript[] enemySpawnPoints;
    public RatTrailScript[] ratTrails;

    [HideInInspector]
    public bool arrived = false;

    // unique identifier for this waypoint.
    public string waypointID;

    public static WaypointScript firstBossWaypoint = null;

    public int remainingEnemies
    {
        get
        {
            int total = 0;
            foreach (enemySpawnerScript spawnPoint in enemySpawnPoints)
            {
                total += spawnPoint.enemiesLeft;
                if (spawnPoint.occupied != null)
                {
                    total++; //currently active enemy
                }
            }
            return total;
        }
    }
	
    public bool CheckTag(MovementStates state)
    {
        //TODO: change tag to a property of this class
        if (state == MovementStates.HOLDING) 
            return CompareTag("Holding");
        if (state == MovementStates.MOVING)
            return CompareTag("Moving");
        if (state == MovementStates.SLIDING)
            return CompareTag("Sliding");
        return false;
    }

    public void arrive()
    {
        arrived = true;
        EventManagerScript.FireEvent(ArriveWaypointEvent.NAME, new ArriveWaypointEvent(waypointID));
        if (this.waypointID.StartsWith("BossFight") && WaypointScript.firstBossWaypoint == null)
        {
            firstBossWaypoint = this; // make sure that only the first BossFight trigger event.
            EventManagerScript.FireEvent(ArriveAtBossStageEvent.NAME, new ArriveAtBossStageEvent());
        }
        for (int i = 0; i < enemySpawnPoints.Length; i++)
        {
            // TODO: change to use the event system.
            enemySpawnerScript spawnPoint = enemySpawnPoints[i].GetComponent<enemySpawnerScript>();
            spawnPoint.playerArrived = true;
            spawnPoint.waypointID = this.waypointID;
        }
            
        foreach (RatTrailScript ratTrail in ratTrails)
        {
            ratTrail.Schedule();
        }

        // TODO fire PlayArriveEvent
    }

    // call this function before entering a way point, to clean up enemies.
    // Do not call it after entering a waypoint, it will clean up the current waypoint too.
    public void ClearEnemies()
    {
        foreach (enemySpawnerScript spawnPoint in enemySpawnPoints)
        {
            spawnPoint.enemiesLeft = 0;
        }

        //clean all the enemies, no matter when they are created. 
        
        foreach (Transform enemy in enemyContainerScript.Instance.transform)
        {
            Destroy(enemy.gameObject);
        }
        // TODO: clean up rats too
    }
	// Update is called once per frame
	void Update ()
    {

	}
}
