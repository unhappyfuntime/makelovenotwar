﻿using UnityEngine;
using System.Collections;

public class EnemyColliderScript : MonoBehaviour, ICollider {

    public enum LOCATION
    {
        HEAD,
        BODY
    }
    public LOCATION location;
    public EnemyTargetScript targetScript;

    public void Init(EnemyTargetScript targetScript, LOCATION location)
    {
        this.location = location;
        this.targetScript = targetScript;

    }

    public void OnHit(Collider collider, int damage)
    {
        this.targetScript.OnHit(this.location);
    }

}
