﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunshotSpriteScript : MonoBehaviour {

    int gunshotTimer;
    public bool hit;
    string gunshotSprite;

	// Use this for initialization
	void Start () {
        gunshotTimer = 0;
        if (hit)
            gunshotSprite = "gunshot";
        else
            gunshotSprite = "gunMiss";
	}
	
	// Update is called once per frame
	void Update () {
        gunshotTimer++;
        if (gunshotTimer >= 9)
            Destroy(gameObject);
        if (gunshotTimer % 1 == 0)
        {
            GetComponent<Image>().sprite = Resources.Load<Sprite>(gunshotSprite + "/Frame " + (gunshotTimer));
        }
	}
}
