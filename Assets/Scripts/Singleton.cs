﻿using UnityEngine;

/** 
 * Any class that extends this class will be a singleton
 */
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance; // Never use this variable directly outside of this class.

    /**
       Returns the instance of this singleton.
        Make sure to use "Instance" but not "instance". For some reason, sometimes protected variable is accessible too.
    */
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (T)FindObjectOfType(typeof(T));
                if (_instance == null)
                {
                    Debug.LogWarning("An instance of " + typeof(T) +
                       " is needed in the scene, but there is none.");
                }
            }

            return _instance;
        }
    }

    public static bool SafeCheckExistence()
    {
        if (_instance != null)
        {
            return true;
        }
        _instance = (T)FindObjectOfType(typeof(T));
        if (_instance != null)
        {
            return true;
        }

        return false;
    }
}