﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameOverMenuScript : MonoBehaviour {

    public Text outcomeText;
    public GameObject score;
    public Text scoreText;
    public GameObject time;
    public Text timeText;
    public Text timeScoreText;
    public GameObject total;
    public Text totalText;
    // Add script to continue level from same point
    public GameObject continueButton;
    public GameObject restartButton;
    public GameObject proceedButton;

    public void SetOutcome(GameOutcome outcome, int score)
    {
        switch (outcome)
        {
            case GameOutcome.WIN: // Hard code level ID in GameManagerScript
                outcomeText.text = "Level Cleared";
                StartCoroutine(PlayOutcome(outcome));
                break;
            case GameOutcome.LOST:
                outcomeText.text = "DEFEAT";
                break;
        }
        HUDManagerScript.Instance.UICanvas.GetComponent<CanvasGroup>().alpha = 0;
        scoreText.text = "" + score;
        timeText.text = "" + GameManagerScript.Instance.timePlayed;
        // Based on level; hard code it along with level ID
        //  300 is level 1's maximum time
        timeScoreText.text = "" + (int)((300f - GameManagerScript.Instance.timePlayed) * 500);
        totalText.text = "" + (score + (int)((300f - GameManagerScript.Instance.timePlayed) * 500));
    }

    IEnumerator PlayOutcome(GameOutcome outcome)
    {
        if (outcome == GameOutcome.WIN)
        {
            yield return new WaitForSeconds(1f);
            AudioManagerScript.Instance.RequestFX(0, PlayerMovementScript.Instance.transform);
            score.GetComponent<CanvasGroup>().alpha = 1;
            yield return new WaitForSeconds(1f);
            AudioManagerScript.Instance.RequestFX(0, PlayerMovementScript.Instance.transform);
            time.GetComponent<CanvasGroup>().alpha = 1;
            yield return new WaitForSeconds(1f);
            AudioManagerScript.Instance.RequestFX(0, PlayerMovementScript.Instance.transform);
            total.GetComponent<CanvasGroup>().alpha = 1;
            proceedButton.GetComponent<CanvasGroup>().alpha = 1;
        } else
        {
            yield return new WaitForSeconds(1f);
            AudioManagerScript.Instance.RequestFX(0, PlayerMovementScript.Instance.transform);
            score.GetComponent<CanvasGroup>().alpha = 1;
            yield return new WaitForSeconds(1f);
            AudioManagerScript.Instance.RequestFX(0, PlayerMovementScript.Instance.transform);
            continueButton.GetComponent<CanvasGroup>().alpha = 1;
            yield return new WaitForSeconds(1f);
            AudioManagerScript.Instance.RequestFX(0, PlayerMovementScript.Instance.transform);
            restartButton.GetComponent<CanvasGroup>().alpha = 1;
        }
    }
}
