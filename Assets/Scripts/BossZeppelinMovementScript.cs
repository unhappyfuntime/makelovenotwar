﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossZeppelinMovementScript : Singleton<BossZeppelinMovementScript> {
    public int currentWaypointIndex = -1;
    public Transform waypointContainer;
    public List<BossWaypointScript> waypoints;

    private bool started = false;

    public BossWaypointScript currentWaypoint
    {
        get
        {
            return waypoints[currentWaypointIndex];
        }
    }

    // Use this for initialization
    void Start () {
        foreach (Transform child in waypointContainer)
        {
            waypoints.Add(child.GetComponent<BossWaypointScript>());
        }
        EventManagerScript.AddEventListener(ArriveAtBossStageEvent.NAME, StartMoving);
        // IMPORTAT: zeppelin should be enabled when the game start in order to register this listener.
        this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
	}
	

	// Update is called once per frame
	void Update () {
        if (!started)
        {
            return;
        }
        if (currentWaypoint.type == BossWaypointType.MOVE)
        {
            BossWaypointScript nextWaypoint = waypoints[currentWaypointIndex + 1];
            if (!WaypointHelper.NextWayPointReached(
                currentWaypoint.transform,
                nextWaypoint.transform,
                transform,
                currentWaypoint.moveSpeed,
                currentWaypoint.turnSpeed))
            {
                
                WaypointHelper.Step(transform, nextWaypoint.transform, currentWaypoint.moveSpeed, currentWaypoint.turnSpeed);
            } else
            {
                //Debug.Log("NextWaypoint reached");
                currentWaypointIndex++;
                nextWaypoint.Arrive(transform);
            }
        }
            
	}

    void StartMoving(BaseEvent e)
    {
        this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
        started = true;
        JumpToID("Phase1_enter");
       
    }

    public void JumpToID(string waypointID)
    {
        //Debug.Log("try jumping to id " + waypointID);
        for (int i = 0; i< waypoints.Count; i++)
        {
            //Debug.Log("waypintId is " + waypoints[i].waypointID);
            if (waypoints[i].waypointID == waypointID)
            {
                //Debug.Log(waypoints[i].waypointID + " is the same as  " + waypointID);
                currentWaypointIndex = i;
                //Debug.Log("boss jump to " + currentWaypoint.name);
                currentWaypoint.Arrive(transform);
                return;
            }
        }

        // couldn't find the waypoint
        Debug.LogWarning("Cannot find the waypointID " + waypointID);
    }
}
