﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStatScript : Singleton<BossStatScript>
{

    public int maxHP = 100;
    public int HP = 100;
    public bool isAlive = true;
    public bool activated = false;

    // Activate this component when player reaches the boss stage.
    void Start()
    {
        EventManagerScript.AddEventListener(ArriveAtBossStageEvent.NAME, this.OnArrive);
    }

    void OnArrive(BaseEvent e)
    {
        // initialize the hp bar
        EventManagerScript.FireEvent(BossHPChangeEvent.NAME, new BossHPChangeEvent(GetHPPercent()));
        EventManagerScript.AddEventListener(BossHPChangeEvent.NAME, OnBossHPChange);
        EventManagerScript.AddEventListener(BossHitEvent.NAME, this.OnBossHit);
        //Debug.Log("boss stat added listener");
        activated = true;
    }

    void OnBossHPChange(BaseEvent e)
    {
        //Debug.Log("OnBoss hp change");
        float hpPercent = (e as BossHPChangeEvent).currentHPPercent;
        //Debug.Log("hp percent is " + hpPercent);
        if (hpPercent <= 0)
        {
            //Debug.Log("fire boss die event");
            EventManagerScript.RemoveEventListener(BossHPChangeEvent.NAME, this.OnBossHPChange);
            EventManagerScript.RemoveEventListener(BossHitEvent.NAME, this.OnBossHit);
            isAlive = false;
            EventManagerScript.FireEvent(BossDieEvent.NAME, new BossDieEvent());
        }
    }

    void OnBossHit(BaseEvent e)
    {
        BossHitEvent _event = e as BossHitEvent;
        HP -= _event.damage;
        EventManagerScript.FireEvent(BossHPChangeEvent.NAME, new BossHPChangeEvent(GetHPPercent()));
    }

    public float GetHPPercent()
    {
        return (float)HP / (float)maxHP;
    }

}
