﻿using UnityEngine;

public interface ICollider {
    void OnHit(Collider collider, int damage);
}
