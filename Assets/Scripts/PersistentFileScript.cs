﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PersistentFileScript : Singleton<PersistentFileScript> {

    public static PersistentData data = new PersistentData();
    private static string filename = "/playerData.dat";

    void Awake()
    {
        if (PersistentFileScript.Instance != this)
        {
            Destroy(this);
            return;
        }
        DontDestroyOnLoad(gameObject);
        PersistentFileScript.Load();
    }
    
    public static void Load()
    {
        string fullPath = Application.persistentDataPath + filename;
        if (File.Exists(fullPath)) {
            FileStream file = File.Open(fullPath, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            data = (PersistentData)bf.Deserialize(file);
            //Debug.Log("Load success");
            file.Close();
        }

    }

    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + filename);
        bf.Serialize(file, data);
        //Debug.Log("Save success");
        file.Close();
    }
}

[Serializable]
public class PersistentData
{
    public int lvl1HighScore = 0;
    public int lvl2HighScore = 0;
    
    public void TryUpdateHighScore(string scene, int score)
    {
        switch (scene)
        {
            case "1-1":
                lvl1HighScore = score > lvl1HighScore ? score : lvl1HighScore;
                break;
            case "2-1":
                lvl2HighScore = score > lvl2HighScore ? score: lvl2HighScore;
                break;
        }
    }
}
