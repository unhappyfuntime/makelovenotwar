﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour, ICollider {

    public bool isDropping = false;
    bool warned = false;
    public Vector3 from;
    public Vector3 to;
    public float speed = 5.0f;

    public float totalDist;

    private Vector3 movingDirection;
	// Update is called once per frame

    void Start()
    {
        AudioManagerScript.Instance.RequestFX(8, 2.5f, GameManagerScript.Instance.Player.transform);
    }

    bool IsReaching()
    {
        Vector3 currentToTarget = to - this.transform.position;
        Vector3 targetToNext = this.transform.position + TransformSpeed() * this.movingDirection - to;
        if (Vector3.Dot(currentToTarget, targetToNext) >= 0 || currentToTarget.magnitude <= Constants.EPSILON)
        {
            return true;
        }
        return false;
    }

    /**
     * The bomb feels so fast when it is close to the player. 
     * This function tunes the speed down so that player has time to respond.
     *
     * Screw that, add the damn warning
     */
    float TransformSpeed()
    {
        //float traveledDistance = Vector3.Distance(from, this.transform.position);
        //float traveledRatio = traveledDistance / this.totalDist;
        return speed * 0.3f;
    }

	void Update () {
		if (isDropping && GameManagerScript.gameRunning)
        {
            if (!IsReaching())
            {
                this.transform.position = this.transform.position + TransformSpeed() * this.movingDirection;
            } else
            {
                EventManagerScript.FireEvent(BombLandedEvent.NAME, new BombLandedEvent());
                HitPlayer();
                Destroy(gameObject);
            }
        }
        if (!warned)
        {
            Vector3 currentToTarget = to - this.transform.position;
            if (currentToTarget.magnitude <= 20)
            {
                AudioManagerScript.Instance.RequestFX(7, 0.6f, GameManagerScript.Instance.Player.transform);
                EventManagerScript.FireEvent(AccurateBulletFireEvent.NAME, new AccurateBulletFireEvent());
                warned = true;
            }
        }
	}

    void HitPlayer()
    {
        AudioManagerScript.Instance.RequestFX(3, GameManagerScript.Instance.Player.transform);
        EventManagerScript.FireEvent(AccurateBulletGoneEvent.NAME, new AccurateBulletFireEvent());
        if (PlayerMovementScript.Instance.isInAction && !GameManagerScript.Instance.invincible && GameManagerScript.gameRunning)
        {
            //TODO Since the bomb moves at a very slow speed when it is close to player.
            //There is a delay between bomb getting close enough and bomb actually exploding.
            GameManagerScript.Instance.getHit();
        }
        Destroy(gameObject);
    }
    public void Drop(Vector3 from, Vector3 to)
    {
        if (isDropping)
        {
            Debug.LogError("is already dropping");
        }

        isDropping = true;
        this.from = from;
        this.to = to;
        this.totalDist = Vector3.Distance(to, from);

        this.transform.position = from;
        this.movingDirection = (to - from).normalized;
    }

    public void OnHit(Collider collider, int damage)
    {
        Destroy(gameObject);
    }
}
