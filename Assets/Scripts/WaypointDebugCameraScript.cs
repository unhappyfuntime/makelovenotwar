﻿using UnityEngine;
using System.Collections;

public class WaypointDebugCameraScript : Singleton<WaypointDebugCameraScript> {


    public Camera gameCamera;
    private Camera debugCamera;
    public bool isActive;

    
	// Use this for initialization
	void Awake () {
        debugCamera = GetComponent<Camera>();
        isActive = gameObject.activeSelf;
        gameObject.SetActive(false);
	}

	public void MoveTo(Vector3 position)
    {
        this.debugCamera.transform.position = position;
    }

    public void RotateTo(Quaternion quaternion)
    {
        this.debugCamera.transform.rotation = quaternion;
    }

    public void setActive(bool active)
    {
        gameObject.SetActive(active);
        gameCamera.gameObject.SetActive(!active);
        isActive = gameObject.activeSelf;
    }

}
