﻿using UnityEngine;
using System.Collections;

public class enemyScript : MonoBehaviour {

    // For title screen only
    public bool dance = false;

    public int enemyType;
    public int health;
    public int posthit = 0;

    // Accuracy is counted 0 - 100
    public int accuracy;

    [HideInInspector]
    public bool hiding;
    bool dying;

    [HideInInspector]
    public GameObject sitLocation; // spawner location

    bool arrived;
    bool flinching;

    public int clipSize;
    int clip;
    bool shooting;

    public int resource;

    public Animator animator;

    public Transform[] bullets;

	void Start ()
    {
        hiding = false;
        arrived = false;
        shooting = false;
        flinching = false;
        dying = false;
        clip = clipSize;
        
        if (dance)
            return;

        animator.SetBool("Running", true);
	}

    public bool getHit(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            if (resource == 1 && posthit < 3)
            {
                int supply = (int)Random.Range(1f, 3.25f);
                switch (supply)
                {
                    case 1: PlayerAmmoScript.Instance.Supply(supply, 25); break;
                    case 2: PlayerAmmoScript.Instance.Supply(supply, 4); break;
                    case 3: PlayerAmmoScript.Instance.Supply(supply, 1); break;
                }
            }
            if (resource == 2 && posthit == 0)
                HUDManagerScript.Instance.heal();
            animator.SetBool("Death", true);
            animator.SetBool("Flinch", true);
            animator.Play("Death");
            Die();
            posthit++;
            return posthit < 4;
        }
        animator.SetBool("Flinch", true);
        animator.SetInteger("HitType", Random.Range(0,1));
        flinching = true;
        StartCoroutine(flinch());
        return true;
    }

    IEnumerator flinch()
    {
        yield return new WaitForSeconds(0.5f);
        flinching = false;
        animator.SetBool("Flinch", false);
    }
    public void Die()
    {
        StartCoroutine(die());
        EventManagerScript.FireEvent(EnemyDieEvent.NAME, new EnemyDieEvent(this.enemyType));
        health = -100;
    }
    IEnumerator die()
    {
        if (dying)
            yield break;
        dying = true;
        ParticleManagerScript.Instance.requestParticlePrefab(1, transform.position);
        yield return new WaitForSeconds(0.7f);
        sitLocation.GetComponent<enemySpawnerScript>().occupantDead();
        Destroy(gameObject);
    }


    public Transform SearchHierarchyForBone(Transform current, string name)
    {
        // check if the current bone is the bone we're looking for, if so return it
        if (current.name == name)
            return current;
        // search through child bones for the bone we're looking for
        for (int i = 0; i < current.childCount; ++i)
        {
            // the recursive step; repeat the search one step deeper in the hierarchy
            Transform found = SearchHierarchyForBone(current.GetChild(i), name);
            // a transform was returned by the search above that is not null,
            // it must be the bone we're looking for
            if (found != null)
                return found;
        }

        // bone with name was not found
        return null;
    }

    void shoot()
    {
        // Fire bullet
        Transform bulletOrigin = SearchHierarchyForBone(transform, "Bip01_R_Hand");
        if (bulletOrigin != null)
        {
            float bullethit = Random.Range(0, 100);
            if (bullethit < accuracy)
            {
                // lethal bullet
                var bullet = Instantiate(bullets[1]) as Transform;
                bullet.position = bulletOrigin.position;
                bullet.GetComponent<bulletScript>().direction = (PlayerMovementScript.Instance.transform.position - 
                    bullet.position).normalized;
                
            } else
            {
                var bullet = Instantiate(bullets[0]) as Transform;
                bullet.position = bulletOrigin.position;
                bullet.GetComponent<bulletScript>().direction = ((PlayerMovementScript.Instance.transform.position - 
                    bullet.position) + new Vector3(Random.Range(-2, 2), Random.Range(-2, 2), Random.Range(-2, 2))).normalized;
            }
            // bullet.SetParent(transform);
        }

        clip--;
        StartCoroutine(shootDecision());
    }

    IEnumerator shootDecision()
    {
        yield return new WaitForSeconds(0.8f);
        hiding = true;
        animator.SetBool("Hiding", true);
        animator.SetBool("Shoot", false);
        if (clip == 0)
        {
            yield return new WaitForSeconds(3.2f);
            shooting = false;
            clip = clipSize;
            yield break;
        }
        else if (Random.Range(0f, 10f) < 7)
        {
            shooting = false;
            yield break;
        }
        yield return new WaitForSeconds(1.6f);
        shooting = false;
    }

	// Update is called once per frame
	void Update () {
        // For the title screen
        if (dance)
        {
            animator.SetBool("Dance", true);
            return;
        }

        if (health == -100)
            return;
        if (health <= 0 && health > -100)
        {
            Die();
            return;
        }
        if (flinching)
            return;

        if (!arrived)
        {
            transform.position = Vector3.MoveTowards(transform.position, sitLocation.transform.position, Time.deltaTime * 4f);
            if ((sitLocation.transform.position - transform.position).magnitude > 0)
            {
                transform.rotation = Quaternion.Slerp(
                transform.rotation,
                Quaternion.LookRotation(sitLocation.transform.position - transform.position, Vector3.up),
                Time.deltaTime * 7);
            }
            
            if (Vector3.Distance(sitLocation.transform.position, transform.position) < 0.1)
            {
                arrived = true;
                hiding = true;
                animator.SetBool("Hiding", true);
                animator.SetBool("Running", false);
                transform.position = sitLocation.transform.position;
            }
            return;
        }

        transform.rotation = Quaternion.Slerp(
            transform.rotation,
            sitLocation.transform.rotation,
            Time.deltaTime * 7);

        if (!shooting && clip > 0)
        {
            hiding = false;
            animator.SetBool("Hiding", false);
            animator.SetBool("Shoot", true);
            shooting = true;
            shoot();
        }

    }
}
