﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleTankSpawnerScript : MonoBehaviour {

    int spawnTime;
    public Transform tank;

	// Use this for initialization
	void Start () {
        spawnTime = Random.Range(240, 480);
    }

    void FixedUpdate()
    {
        spawnTime--;
        if (spawnTime <= 0)
        {
            spawnTime = Random.Range(360, 1080);
            var spawn = Instantiate(tank) as Transform;
            spawn.SetParent(transform);
            spawn.localPosition = Vector3.zero;
            spawn.localScale = Vector3.one;
        }
    }

    // Update is called once per frame
    void Update () {

	}
}
