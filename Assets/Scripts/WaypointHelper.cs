﻿using UnityEngine;

class WaypointHelper
{
    public static bool NextWayPointReached(
        Transform currentWayPoint, 
        Transform nextWayPoint, 
        Transform currentTransform,
        float moveSpeed,
        float rotateSpeed)
    {
        bool finishedRotation = Quaternion.Angle(
            currentTransform.rotation, 
            nextWayPoint.rotation) < 1;
        Vector3 movingDirection = GetDirection(currentTransform, nextWayPoint);
        Vector3 nextPosition = movingDirection * moveSpeed + currentTransform.position;
        Vector3 nextWaypointPos = nextWayPoint.position;
        Vector3 currentPositionRelativeToTarget = currentTransform.position - nextWaypointPos;
        Vector3 nextPositionRelativeToTarget = nextPosition - nextWaypointPos;

        bool nextMoveWillPass = Vector3.Dot(currentPositionRelativeToTarget, movingDirection) * Vector3.Dot(nextPositionRelativeToTarget, movingDirection) <= 0;
        //bool isAlreadyPassed = Vector3.Dot(movingDirection, currentPositionRelativeToTarget) <= 0;
        return nextMoveWillPass && finishedRotation;

    }

    public static Vector3 GetDirection(Transform from, Transform to)
    {
        return (to.position - from.position).normalized;
    } 

    public static void Step(
        Transform currentTransform,
        Transform nextWaypoint,
        float moveSpeed, 
        float turnSpeed
        )
    {
        Vector3 direction = GetDirection(currentTransform, nextWaypoint);
        currentTransform.rotation = Quaternion.Slerp(
                    currentTransform.rotation,
                   nextWaypoint.rotation,
                    Time.deltaTime * turnSpeed);
        float moveStep = Mathf.Min(moveSpeed, Vector3.Distance(currentTransform.position, nextWaypoint.position));
        currentTransform.position += direction *moveStep;
    }

    
}
