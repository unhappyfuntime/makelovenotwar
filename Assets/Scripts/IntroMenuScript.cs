﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroMenuScript : MonoBehaviour {
    public Text lvl1Score;
    public Text lvl2Score;

	// Use this for initialization
	void Start () {
        lvl1Score.text = "" + PersistentFileScript.data.lvl1HighScore;
        lvl2Score.text = "" + PersistentFileScript.data.lvl2HighScore;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
