﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;


public class EventManagerScript : Singleton<EventManagerScript> {
    public class UnityEventWithOneArgument : UnityEvent<BaseEvent> { }

    private Dictionary<string, UnityEventWithOneArgument> eventDictionary;

    void Awake()
    {
        if (eventDictionary == null)
        {
            //Debug.Log("dictionary created");
            eventDictionary = new Dictionary<string, UnityEventWithOneArgument>();
        }
    }

    public static void AddEventListener(string eventName, UnityAction<BaseEvent> listener)
    {
        UnityEventWithOneArgument thisEvent = null;
        if (Instance == null)
        {
            Debug.LogWarning("Instance is null");
        }
        if (Instance.eventDictionary == null)
        {
            Debug.LogWarning("event dictionary is null");
        }
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEventWithOneArgument();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void RemoveEventListener(string eventName, UnityAction<BaseEvent> listener)
    {
        if (Instance == null) return;
        UnityEventWithOneArgument thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void RemoveAllEventListeners(string eventName)
    {
        if (Instance == null) return;
        UnityEventWithOneArgument thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveAllListeners();
        }
    }

    public static void FireEvent(string eventName, BaseEvent eventObj)
    {
        UnityEventWithOneArgument thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(eventObj);
        }
    }
}
