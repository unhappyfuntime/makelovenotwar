﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is just a simple marker component.
// Objects with this component active are invinsible.
// if this component is inactive, doesn't affect.
// See bulletScript for usage
public class InvinsibleScript : MonoBehaviour {
    public bool isInvinsible = false;
}
