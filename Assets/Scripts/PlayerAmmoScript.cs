﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerAmmoScript : Singleton<PlayerAmmoScript>
{

    public int gunIndex;
    public bool canShoot {
        get {
            return PlayerMovementScript.Instance.isInAction;
        }
    }
    public Gun[] guns = new Gun[] { new HeartShooter(), new HeartStream(), new PassionBlaster(), new LoveBomb() };

    public Gun gun
    {
        get
        {
            return guns[gunIndex];
        }
    }


    public void Init()
    {
        gunIndex = 0; //make sure the guns array is initialized
        //Debug.Log("gun is " + gun);
        gun.Equip();
    }
    
    public void Supply(int index, int amount)
    {
        guns[index].Supply(amount);
    }

    public void SwapGun()
    {
        gunIndex = ( gunIndex + 1 ) % guns.Length;
        gun.Equip();
        if (!gun.HasBullets)
        {
            SwapGun(); 
            // IMPORTANT: here we make sure player can always get a loaded gun with one swap, 
            // so make sure there is always a infinite gun. Otherwise might go into infinite loop
        }
    }

    public bool Shoot(Vector3 mousePosition) {
        return canShoot && gun.Shoot(mousePosition);
    }
}
