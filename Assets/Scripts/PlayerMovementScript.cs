﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MovementStates { MOVING, HOLDING, SLIDING }; // these are corresponding the the waypoint tags.
public class PlayerMovementScript : Singleton<PlayerMovementScript> {

    public GameObject holdPosContainer;
    public List<WaypointScript> waypoints;

    public bool isInAction = false; // true when player can shoot and can get hit (not hiding)

    int currentWaypointIndex; //current waypoint is the one player is currently on or the one that player just left.

    public WaypointScript currentWaypoint
    {
        get
        {
            if (currentWaypointIndex >= waypoints.Count)
            {
                return null;
            }
            return waypoints[currentWaypointIndex];
        }
    }
    // States
    public MovementStates movementState;

    // Additional states
    bool proceed; // true when the player is ready to move to next location
    public bool hiding; // true when the player is in the hiding mode, or is in the transition from standing to hiding.
    //bool sliding; // true when the player is moving while having control

    //Vector3 clonePosition;
    //Vector3 direction;
    float distance;
    float turnSpeed;
    float moveSpeed;
    

    private Transform cameraTransform;

	// Use this for initialization
	void Start () {

        initWaypoints();
        //direction = new Vector3(0, 0, 0);
        turnSpeed = 0;
        moveSpeed = 0;
        currentWaypointIndex = 0;
        proceed = true;
        hiding = false;
        if (waypoints == null || waypoints[0] == null) {
            Debug.LogError("ERROR: Camera positions not organized.");
            Debug.LogError("Please check script load order.");
            Application.Quit();
        }

        cameraTransform = GetComponentInChildren<Camera>().transform;

        transform.position = waypoints[0].transform.position;
        transform.rotation = waypoints[0].transform.rotation;
        switch (waypoints[0].tag)
        {
            case "BossHold":
                ToBoss(0);
                break;
            case "Holding":
                ToHolding(0);
                break;
            case "Moving":
                ToMoving(0);
                break;
            case "Sliding":
                ToSliding(0);
                break;
        }
    }

    void initWaypoints()
    {
        foreach (Transform child in holdPosContainer.transform)
        {
            WaypointScript childWaypoint = child.GetComponent<WaypointScript>();
            if (childWaypoint)
            {
                waypoints.Add(childWaypoint);
            }
        }
    }

	// Update is called once per frame
	void Update () {

        // Handle moving
        if (movementState == MovementStates.MOVING || movementState == MovementStates.SLIDING)
        {
            if (NextWayPointReached())
            {
                //moving = false;
                int nextWaypoint = currentWaypointIndex + 1;
                switch (waypoints[nextWaypoint].tag)
                {
                    case "BossHold":
                        ToBoss(nextWaypoint);
                        break;
                    case "Holding":
                        ToHolding(nextWaypoint);
                        break;
                    case "Moving":
                        ToMoving(nextWaypoint);
                        break;
                    case "Sliding":
                        ToSliding(nextWaypoint);
                        break;
                }
                waypoints[nextWaypoint].arrive();
                // clean all the enemies left from last waypoint.
                // this is necessary for slide. since it doesn't require killing all the enemies
                waypoints[currentWaypointIndex].ClearEnemies();
                currentWaypointIndex = nextWaypoint;
                //moveSpeed = Mathf.Min(moveSpeed, DistanceToNextWaypoint());

            }
            else // still on the way
            {
                WaypointHelper.Step(transform, waypoints[currentWaypointIndex + 1].transform, moveSpeed, turnSpeed);
            }
        }


        // Handle Hiding
        if (hiding)
        {
            if (Vector3.Distance(cameraTransform.localPosition, currentWaypoint.hidingPos) > 0.05f)
            {
                cameraTransform.localPosition = Vector3.MoveTowards(cameraTransform.localPosition, currentWaypoint.hidingPos, Time.deltaTime * 5);
            }
        }

        // Handle Showing
        if (!hiding)
        {
            if (Vector3.Distance(cameraTransform.localPosition, Vector3.zero) > 0.05f)
            {
                cameraTransform.localPosition = Vector3.MoveTowards(cameraTransform.localPosition, Vector3.zero, Time.deltaTime * 5);
            }
        }

    }
    public void DebugNextPoint()
    {
        if (HasNextWaypoint())
        {
            var nextWaypointIndex = currentWaypointIndex + 1;
            movementState = MovementStates.MOVING;
            transform.position = waypoints[nextWaypointIndex].transform.position;
            transform.rotation = waypoints[nextWaypointIndex].transform.rotation;
            turnSpeed = 0;
            moveSpeed = 0;
            hiding = true;
            GameManagerScript.Instance.PauseTimer();
        }
    }
    // state change functions

    private void OnProceed(BaseEvent e)
    {
        EventManagerScript.RemoveEventListener(ProceedEvent.NAME, OnProceed);
        Proceed();
    }

    private void ToHolding(int waypointHere)
    {
        isInAction = true;
        movementState = MovementStates.HOLDING;
        proceed = true;
        turnSpeed = 0;
        moveSpeed = 0;
        ToNotHiding();
        GameManagerScript.Instance.timeLeft = GameManagerScript.Instance.initTime;
        GameManagerScript.Instance.ResumeTimer();
    }
    private void ToBoss(int waypointHere)
    {
        //Debug.Log("toBoss");
        movementState = MovementStates.HOLDING;
        proceed = true;
        turnSpeed = 0;
        moveSpeed = 0;
        ToNotHiding();
        GameManagerScript.Instance.timeLeft = 99f;
        GameManagerScript.Instance.ResumeTimer();
        EventManagerScript.AddEventListener(ProceedEvent.NAME, OnProceed);
    }

    private void ToMoving(int waypointHere)
    {
        
        movementState = MovementStates.MOVING;
        proceed = false;
        turnSpeed = waypoints[waypointHere].turnSpeed;
        moveSpeed = waypoints[waypointHere].moveSpeedFraction;
        //direction = GetDirection(waypointHere, waypointHere + 1);
        ToNotHiding();
        isInAction = false;
        GameManagerScript.Instance.PauseTimer();
    }

    private void ToSliding(int waypointHere)
    {
        
        movementState = MovementStates.SLIDING;
        proceed = false;
        turnSpeed = waypoints[waypointHere].turnSpeed;
        moveSpeed = waypoints[waypointHere].moveSpeedFraction;
        //direction = GetDirection(waypointHere, waypointHere + 1);
        ToNotHiding();
        GameManagerScript.Instance.timeLeft = GameManagerScript.Instance.initTime;
        GameManagerScript.Instance.ResumeTimer();
        isInAction = true;
    }

    private void ToHiding()
    {
        if (movementState == MovementStates.HOLDING || movementState == MovementStates.SLIDING)
        {
            hiding = true;
            isInAction = false;
        }
    }

    private void ToNotHiding()
    {
        hiding = false;
        if (movementState == MovementStates.HOLDING || movementState == MovementStates.SLIDING)
        {
            isInAction = true;
        }
        
    }

    public bool Proceed()
    {
        if (proceed && HasNextWaypoint())
        {
            //Debug.Log("currentwaypoint is " + currentWaypoint.gameObject.name);
            currentWaypoint.ClearEnemies();
            ToMoving(currentWaypointIndex);
            return true;
        }
        return false;
    }
    // User controll handling

    public void OnNextDown()
    {
        Proceed();
    }

    /**
     * Called by PlayerKeyboardEvent
     */
    public void OnHideDown()
    {
        ToHiding();
        
    }

    /**
     * Called by PlayerKeyboardEvent
     */
    public void OnHideUp()
    {
        hiding = false;
        ToNotHiding();
    }


    // Helper functions

    private bool HasNextWaypoint()
    {
        return waypoints.Count > currentWaypointIndex + 1;
    }

    private float DistanceToNextWaypoint()
    {
        if (HasNextWaypoint())
        {
            return Vector3.Distance(transform.position, waypoints[currentWaypointIndex + 1].transform.position);
        } else
        {
            Debug.LogError("Checking distance to the next waypoint while there is no next waypoint");
            return 0;
        }
    }

    private Vector3 GetDirection(int waypointA, int waypointB)
    {
        return (waypoints[waypointB].transform.position - waypoints[waypointA].transform.position).normalized;
    }

    private bool NextWayPointReached()
    {
        if (HasNextWaypoint())
        {
            return WaypointHelper.NextWayPointReached(
                waypoints[currentWaypointIndex].transform, 
                waypoints[currentWaypointIndex + 1].transform, 
                transform,
                moveSpeed, 
                turnSpeed);
        }else
        {
            Debug.LogError("Shouldn't be checking if next waypoint is reached, because there is no next waypoint");
            return false;
        }
    }

    public void DebugToBoss()
    {
        //Debug.Log("Debugging to boss");
        WaypointScript bossWaypoint = null;
        int bossFightPointIndex = -1;

        // find the boss waypoint.
        for (int i = 0; i < waypoints.Count; i++)
        {
            if (waypoints[i].waypointID.StartsWith(Constants.BOSS_FIGHT_PREFIX))
            {
                bossFightPointIndex = i;
                break;
            }
        }

        if (bossFightPointIndex > -1)
        {
            //Debug.Log("found boss waypoint at " + bossFightPointIndex);
            bossWaypoint = this.waypoints[bossFightPointIndex];
            // clear all the enemies in any previous waypoints
            foreach (WaypointScript waypoint in waypoints)
            {
                if (waypoint != bossWaypoint)
                {
                    waypoint.ClearEnemies();
                } else
                {
                    break;
                }
            }
            transform.position = bossWaypoint.transform.position;
            transform.rotation = bossWaypoint.transform.rotation;
            moveSpeed = 0;
            turnSpeed = 0;
            currentWaypointIndex = bossFightPointIndex;

            GameManagerScript.Instance.timeLeft = 99f;
            GameManagerScript.Instance.ResumeTimer();
            ToBoss(bossFightPointIndex);
            bossWaypoint.arrive();
        }
    }
}
