﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatTrailScript : MonoBehaviour {

    public float totalTime = 0;
    public float delay = 0;

    public bool started = false;

    public GameObject ratPrefab;
    private GameObject rat;
    private Transform ratContainer;

    private float speed = 0;
    private float traveledDistance = 0;

    private Transform[] trailPoints;
    private float[] distanceFromOrigin;
    private float[] distanceFromLast;

	// Use this for initialization
	void Start () {
        this.trailPoints = new Transform[transform.childCount];
        this.distanceFromOrigin = new float[this.trailPoints.Length];
        this.distanceFromLast = new float[this.trailPoints.Length];
        int index = 0;
        foreach (Transform child in transform)
        {
            this.trailPoints[index] = child;
            index++;
        }
        // calculate the average speed
        CalculateSpeed();
        ratContainer = transform.parent;
        // debug
        StartRunning();
	}
	void CalculateSpeed ()
    {
        if (trailPoints == null || trailPoints.Length <= 1)
        {
            return;
        }
        distanceFromOrigin[0] = 0;
        distanceFromLast[0] = 0;
        for (int i = 1; i < trailPoints.Length; i ++)
        {
            distanceFromLast[i] = Vector3.Distance(trailPoints[i].position, trailPoints[i - 1].position);
            distanceFromOrigin[i] = distanceFromOrigin[i - 1] + distanceFromLast[i];
        }
        
        speed = distanceFromOrigin[trailPoints.Length - 1] / trailPoints.Length;
    }
	// Update is called once per frame
	void Update () {
        if (started && delay >= 0)
        {
            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                StartRunning();
            }
        } else if (started && trailPoints.Length > 1)
        {
            traveledDistance += Time.deltaTime * speed;
            int lastPointIndex = 0;
            int currentPointIndex = 1;
            while (currentPointIndex < trailPoints.Length)
            {
                if (traveledDistance <= distanceFromOrigin[currentPointIndex])
                {
                    float distanceFromLastPoint = traveledDistance - distanceFromOrigin[lastPointIndex];
                    rat.transform.position = Vector3.Lerp(
                        trailPoints[lastPointIndex].position,
                        trailPoints[currentPointIndex].position,
                        distanceFromLastPoint / distanceFromLast[currentPointIndex]);
                    rat.transform.rotation = Quaternion.LookRotation(
                        trailPoints[currentPointIndex].position - trailPoints[lastPointIndex].position);
                    break;
                }
                lastPointIndex++;
                currentPointIndex++;
            }
            if (lastPointIndex == trailPoints.Length - 1)
            {
                FinishRunning();   
            }
        }
	}

    void StartRunning()
    {
        if (this.trailPoints.Length > 0)
        {
            rat = Instantiate(ratPrefab);
            rat.transform.SetParent(ratContainer);

        }
    }
    void FinishRunning()
    {
        started = false;
        //Debug.Log("decrease player hp here");
    }
    // start the timer. StartRunning when delay pass. 
    public void Schedule()
    {
        started = true;
    }
}
