﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeScript : Singleton<CameraShakeScript>
{

    float shudder = 0;

    public void shake() {
        shudder = 1.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if (shudder > 0)
        {
            transform.localPosition = Random.insideUnitSphere * shudder * 0.5f;
            shudder -= Time.deltaTime;
        } else
        {
            shudder = 0;
        }
    }
}
