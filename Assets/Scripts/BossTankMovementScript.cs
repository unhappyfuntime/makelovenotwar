﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void Callback();

public class BossTankMovementScript : Singleton<BossTankMovementScript> {
    public Transform[] cannons;
    public Transform body;

    public float bodyRotateSpeed = 50.0f;
    public float cannonsRotateSpeed = 100.0f;

    private float bodyFinalDegree = 0;
    private float cannonsFinalDegree = 0;

    private Callback bodyRotationCallback = null;
    private Callback cannonsRotationCallback = null;
   
    public Transform bulletOrigin
    {
        get
        {
            return cannons[1].transform;
        }
    }

	// Use this for initialization
	void Start () {
        
        
	}
	// calculate the smaller absolute value, then apply the sign
    float Smaller(float a, float b, float sign)
    {
        sign = sign < 0 ? -1 : 1;
        return Mathf.Min(Mathf.Abs(a), Mathf.Abs(b)) * sign;
    }

    // return degree difference from a to b. sign denote the direction. 
    float DegreeBetween(float a, float b)
    {
        a = (a + 360) % 360;
        b = (b + 360) % 360;
        var difference = Mathf.Abs(a - b);
        if (difference > 180)
        {
            difference = 360 - difference;
        }

        if (Mathf.Abs((a + difference) % 360 - b) < Constants.EPSILON) // < 0.5 prevent floating point error.
        {
            return -difference;
        }
        return difference;
    }
	// Update is called once per frame
	void Update () {
        if (!BossStatScript.Instance.isAlive)
        {
            return;
        }
		if (cannonsFinalDegree != GetCannonsDegree())
        {
            var remainDegree = DegreeBetween(cannonsFinalDegree, GetCannonsDegree());
            var degree = Smaller(remainDegree, cannonsRotateSpeed * Time.deltaTime, remainDegree);
            
            RotateCannons(degree);
            if (degree == remainDegree && cannonsRotationCallback != null)
            {
                // finished rotation
                Callback callback = cannonsRotationCallback;
                cannonsRotationCallback = null;
                callback();
                // assign callback to a local variable, because inside the callback, the global callback might be modified.
            }

        }

        if (bodyFinalDegree != GetBodyDegree())
        {
            var remainDegree = bodyFinalDegree - GetBodyDegree();
            var degree = Smaller(remainDegree, bodyRotateSpeed * Time.deltaTime, remainDegree);
            RotateBody(degree);
            if (degree == remainDegree && bodyRotationCallback != null)
            {
                Callback callback = bodyRotationCallback;
                bodyRotationCallback = null;
                callback();
            }

        }
	}

    float GetBodyDegree()
    {
        
        return body.transform.localEulerAngles.x;
    }

    float GetCannonsDegree()
    {
        return cannons[0].transform.localEulerAngles.x;
    }
    public void Rotate(Transform trans, float degree)
    {
        trans.Rotate(new Vector3(degree, 0, 0), Space.Self);
    }

    public void RotateBody(float degree)
    {
        Rotate(body, degree);
    }

    void RotateCannons(float degree)
    {
        foreach (Transform cannon in cannons)
        {
            Rotate(cannon, degree);
        }
    }

    public void RotateBodyTo(float degree, Callback callback = null)
    {
        bodyFinalDegree = degree;
        if (callback != null)
        {
            bodyRotationCallback = callback;
        }
        
    }
    public void RotateCannonsTo(float degree, Callback callback = null)
    {
        cannonsFinalDegree = degree;
        if (callback != null)
        {
            cannonsRotationCallback = callback;
        }
    }
}
