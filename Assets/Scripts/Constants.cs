﻿public class Constants {
    public const string LVL1_BOSS_WAYPOINT_ID = "BossFight_LV1"; // Make sure to set the same value in the waypointID of the last waypoint in inspector
    public const string LVL2_BOSS_WAYPOINT_ID = "BossFight_LV2"; 
    public const float EPSILON = 0.05f; // very small number, compare this with the difference of two float number, to prevent floating point error.
    public const int PLAYER_DEBUG_BULLET_DAMAGE = 1; // the damage of the bullet, for now, set to 1. 
    public const string BOSS_FIGHT_PREFIX = "BossFight";
}
