﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManagerScript : Singleton<MenuManagerScript> {

    public GameOverMenuScript gameOverMenu;

	// Use this for initialization
	void Start () {
        EventManagerScript.AddEventListener(GameOverEvent.NAME, OnGameOver);
	}
	
    void OnGameOver(BaseEvent e)
    {
        //Debug.Log("game over fired in menu Manager");
        GameOverEvent _event = e as GameOverEvent;
        gameOverMenu.gameObject.SetActive(true);
        gameOverMenu.SetOutcome(_event.outcome, _event.score);
    }
}
