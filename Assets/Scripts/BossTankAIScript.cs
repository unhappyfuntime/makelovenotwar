﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public delegate void UpdateFunc();

public class BossTankAIScript : Singleton<BossTankAIScript> {

    BossTankMovementScript movement;
    
    const float SHOOTING_RANGE = 15; // the degree the tank will rotate during sweeping, from -SHOOTING_RANGE to +SHOOTING_RANGE
    const float LETHAL_SHOOTING_RANGE = 5; // while sweeping, when inside this range, bullets become lethal.

    bool isShooting = false;
    bool bulletLethal = false;

    public Transform normalBullet;
    public Transform lethalBullet;

    public Transform smoke;
    int smokeTimer = 0;

    //public float submergeDepth = 5;
    //public float submergeSpeed = 50;
    bool isHiding = false;
    public int maxPhase = 5;
    [HideInInspector]
    public int phase = 0;
    int bodyCount;

    public enemySpawnerScript[] rangeSpawners;
    public enemySpawnerScript[] closeSpawners;

    public int HP = 40;
    public WaypointScript waypoint;
    float shudder = 0;
    const float SHUDDER_FRAME_RATE = 0.05f;
    Vector3 position;

    public UpdateFunc updateFunc; // the update function that will be called in different phases.

	// Use this for initialization
	void Start () {
        movement = BossTankMovementScript.Instance;
        EventManagerScript.AddEventListener(ArriveWaypointEvent.NAME, this.OnPlayerArrived);
        position = transform.localPosition;
    }

    void OnPlayerArrived(BaseEvent e)
    {
        ArriveWaypointEvent _event = e as ArriveWaypointEvent;
        if (_event.waypointID == Constants.LVL1_BOSS_WAYPOINT_ID)
        {
            // Player arrive at boss waypoint.
            EventManagerScript.AddEventListener(BossHPChangeEvent.NAME, this.OnBossHPDrop);
            EventManagerScript.AddEventListener(BossDieEvent.NAME, this.OnBossDie);
            SummonRangeSoldiers();
            SummonCloseSoldiers();
        }
    }
    bool InBetween(float test, float min, float max)
    {
        return test >= min && test < max;
    }

    void OnBossDie(BaseEvent e)
    {
        // kill all enemy
        //Debug.Log("boss die");
        ClearSoldiers();
        
    }
    void OnBossHPDrop(BaseEvent e)
    {
        BossHPChangeEvent _event = e as BossHPChangeEvent;
        if (InBetween(_event.currentHPPercent, 0.6f, 0.8f) && phase == 0)
        {
            phase = 1;
            Sweep();
        }

        if (InBetween(_event.currentHPPercent, 0.4f, 0.6f) && phase == 1)
        {
            phase = 2;
            updateFunc = Smoke;
            GetComponent<InvinsibleScript>().isInvinsible = true;
            EventManagerScript.FireEvent(BossInvinsibleEvent.NAME, new BossInvinsibleEvent(true));
            int soldierKilled = 0;
            SummonRangeSoldiers();
            SummonCloseSoldiers();
            UnityAction<BaseEvent> enemyDieHandler = null;
            enemyDieHandler = delegate (BaseEvent dieEvent)
            { 
                soldierKilled++; // a closure
                if (soldierKilled > 8) // make sure there are more than 8 enemies 
                {
                    this.phase = 3;
                    GetComponent<InvinsibleScript>().isInvinsible = false;
                    EventManagerScript.FireEvent(BossInvinsibleEvent.NAME, new BossInvinsibleEvent(false));
                    EventManagerScript.RemoveEventListener(EnemyDieEvent.NAME, enemyDieHandler);
                }
                
            };
            EventManagerScript.AddEventListener(EnemyDieEvent.NAME, enemyDieHandler);
        }

        if (InBetween(_event.currentHPPercent, 0.3f, 0.6f) && phase == 3)
        {
            SummonRangeSoldiers();
            SummonCloseSoldiers();
            phase = 4;
            shudder = 3;
            StartCoroutine("Shudder");
        }

        if (InBetween(_event.currentHPPercent, 0, 0.3f) && phase == 4)
        {
            
            phase = 5;
            transform.localPosition = this.position;
            Sweep();
            SummonRangeSoldiers();
            SummonCloseSoldiers();
        }
       
    }
    // boss fight procedure. 
    // boss's there are 5 important points with boss's hp.
    // phase 0, boss summon soldiers, but itself does nothing. 
    // phase 1, when boss's hp drop to 80%. boss does a sweep.
    // phase 2, when boss's hp drop to 60%. boss start smoking, become invinsible. Summon soldiers. 
    // phase 3, when player killed more than 8 soldiers, boss comes back from invisibility.
    // phase 4, right after phase 3 ends, boss shudder, summon soldiers
    // phase 5, when boss's hp drop to 30%, boss sweep and also summon soldiers
	// Update is called once per frame
	void Update () {
        if (BossStatScript.Instance.isAlive && updateFunc != null)
        {
            updateFunc();
        }

       
    }
    public void Smoke()
    {
        // TODO: change to use particle system
        smokeTimer ++;
        if (smokeTimer % 10 == 0)
        {
            var smokey = Instantiate(smoke) as Transform;
            smokey.SetParent(transform);
            smokey.position = transform.position + new Vector3(Random.Range(-1.5f, 1.5f),
                Random.Range(1.5f, 4.5f), Random.Range(-1.5f, 1.5f));
        }
    }

    public IEnumerator Shudder()
    {
        
        while (shudder > 0)
        {
            transform.localPosition = transform.localPosition + Random.insideUnitSphere;
            shudder -= SHUDDER_FRAME_RATE;
            yield return new WaitForSeconds(SHUDDER_FRAME_RATE);
            transform.localPosition = position;
        }
    }
    public bool getHit(int damage)
    {
        if (isHiding)
            return false;
        if (HP < 1)
        {
            GameManagerScript.Instance.bossDead = true;
            return false;
        }
        HP--;
        shudder = 1.5f;
        return true;
    }

    public void StartShooting(bool lethal)
    {
        if (!isShooting)
        {
            isShooting = true;
            StartCoroutine(Shoot());
        }
        bulletLethal = lethal;
    }

    public void StopShooting()
    {
        isShooting = false;
    }


    IEnumerator Shoot()
    {
        while (isShooting )
        {
            yield return new WaitForSeconds(0.2f);
            if (GameManagerScript.gameRunning)
            {
                Transform bulletOrigin = movement.bulletOrigin;
                Transform bullet = null;
                if (bulletLethal)
                {
                    bullet = Instantiate(lethalBullet);
                }
                else
                {
                    bullet = Instantiate(normalBullet);
                }

                bullet.position = bulletOrigin.position;

                bullet.GetComponent<bulletScript>().direction = bulletOrigin.forward;
            }
            
        }
        
    }
    // Actions
    //shoot across the screen horizontally
    public void Sweep ()
    {
        
        StartShooting(false);
        // let's try some callback hell.
        movement.RotateCannonsTo(-SHOOTING_RANGE, () => 
        {
            movement.RotateCannonsTo(-LETHAL_SHOOTING_RANGE, () =>
            {
                StartShooting(true);
                movement.RotateCannonsTo(LETHAL_SHOOTING_RANGE, () =>
                {
                    StartShooting(false);
                    movement.RotateCannonsTo(SHOOTING_RANGE, () =>
                    { 
                        StopShooting();
                        movement.RotateCannonsTo(0);
                    });
                });
            });
                
       });
    }

    public void SummonRangeSoldiers()
    {
        foreach (enemySpawnerScript spawner in rangeSpawners) {
            spawner.enemiesLeft = 2;
            spawner.playerArrived = true;
        }
    }

    public void SummonCloseSoldiers()
    {
        foreach (enemySpawnerScript spawner in closeSpawners)
        {
            spawner.enemiesLeft =2;
            spawner.playerArrived = true;
        }
    }

    public void ClearSoldiers()
    {
        foreach (enemySpawnerScript spawner in rangeSpawners)
        {
            spawner.enemiesLeft = 0;
        }
        foreach (enemySpawnerScript spawner in closeSpawners)
        {
            spawner.enemiesLeft = 0;
        }
        enemyContainerScript.Instance.KillAllEnemies();
    }
    
}
