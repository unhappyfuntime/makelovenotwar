﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleTankScript : MonoBehaviour {

    int heartTime;
    public Transform hearts;

	// Use this for initialization
	void Start () {
        heartTime = Random.Range(10, 40);
        StartCoroutine(disappear());
    }

    IEnumerator disappear()
    {
        yield return new WaitForSeconds(10f);
        Destroy(gameObject);
    }
	
    void FixedUpdate()
    {
        heartTime--;
        if (heartTime <= 0)
        {
            heartTime = Random.Range(10, 40);
            var spawn = Instantiate(hearts) as Transform;
            spawn.position = transform.position + new Vector3(Random.Range(-1.5f, 1.5f),
                Random.Range(1.5f, 4.5f), Random.Range(-1.5f, 1.5f));
            spawn.localScale = new Vector3(20, 20, 20);
        }
    }

	// Update is called once per frame
	void Update () {
        transform.position += new Vector3(30 * Time.deltaTime, 0, 0);
	}
}
