﻿using UnityEngine;
using System.Collections;

public class EnemyTargetScript : MonoBehaviour {
    public Collider headCollider;
    public Collider bodyCollider;

    private enemyScript enemyScript;

    void Start()
    {
        EnemyColliderScript headScript = headCollider.gameObject.AddComponent<EnemyColliderScript>();
        EnemyColliderScript bodyScript = bodyCollider.gameObject.AddComponent<EnemyColliderScript>();
        headScript.Init(this, EnemyColliderScript.LOCATION.HEAD);
        bodyScript.Init(this, EnemyColliderScript.LOCATION.BODY);
        enemyScript = GetComponent<enemyScript>();
    }

    public bool OnHit(EnemyColliderScript.LOCATION location)
    {
        if (location == EnemyColliderScript.LOCATION.HEAD)
        {
            return enemyScript.getHit(3);
        }
        else
        {
            return enemyScript.getHit(1);
        }
    }
}
