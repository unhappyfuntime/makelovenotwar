﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class enemySpawnerScript : MonoBehaviour {

    private List<Transform> spawnNodes = new List<Transform>();

    public int enemiesLeft;

    public int hideAnimType;

    public float delayInSeconds = 0;
    [HideInInspector]
    public enemyScript occupied;
    [HideInInspector]
    public bool playerArrived;

    // TODO: change it so that we assign the waypointID for this spawner in editor.
    // This way we don't need to drag the gameobject to waypoint
    public string waypointID;

	// Use this for initialization
	void Start ()
    {
        occupied = null;
        playerArrived = false;
        foreach (Transform child in transform)
        {
            spawnNodes.Add(child);
        }
	}

    public void occupantDead()
    {
        //occupied = false;
    }

    void spawnEnemy()
    {
        int rand = Random.Range(0, 100); //Random.Range(0, enemyContainerScript.Instance.enemyTypes.Length);
        int enemyType;
        if (rand > 95)
            enemyType = enemyContainerScript.Instance.enemyTypes.Length - 1;
        else if (rand > 88)
            enemyType = enemyContainerScript.Instance.enemyTypes.Length - 2;
        else
            enemyType = Random.Range(0, enemyContainerScript.Instance.enemyTypes.Length - 2);

        var enemy = Instantiate(enemyContainerScript.Instance.enemyTypes[enemyType]) as Transform;
        enemy.SetParent(enemyContainerScript.Instance.transform);
        rand = Random.Range(0, spawnNodes.Count);
        enemy.transform.position = spawnNodes[rand].position;
        enemy.GetComponent<enemyScript>().sitLocation = gameObject;
        occupied = enemy.GetComponent<enemyScript>();
        enemiesLeft--;
    }

	// Update is called once per frame
	void Update ()
    {
        if (!GameManagerScript.gameRunning)
        {
            return;
        }
        if (!playerArrived)
            return;
        if (delayInSeconds > 0)
        {
            delayInSeconds -= Time.deltaTime;
            if (delayInSeconds >= 0)
            {
                return;
            }
        }
        if (occupied)
            return;
        if (enemiesLeft > 0)
        {
            spawnEnemy();
        }
	}
}
