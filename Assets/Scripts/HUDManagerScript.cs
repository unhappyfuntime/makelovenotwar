﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManagerScript : Singleton<HUDManagerScript>
{
    public RectTransform UICanvas;
    public Transform bulletContainer;
    public RectTransform bullet;
    public RectTransform healthContainer;
    public RectTransform health;
    public Text gunText;
    public RectTransform bulletsLeftText;
    public Text timeLeftText;
    public CanvasGroup warning;
    public CanvasGroup critical;
    public RectTransform gunshotSprite;
    public RectTransform gunMissSprite;
    public RectTransform whiteshotSprite;
    public Text scoreText;
    public GameObject scoreStackText;
    public Slider bossHPBar;
    public Image bossHPFill;
    public Camera HUDCamera;

    int scoreStack;
    int scoreStackTimer;
    private GameManagerScript gameManager;
    
    private int warnCounter = 0;
    private int currentlyFlyingAccurateBulletCount = 0;
    private int criticalTimer = 0;

    void Start()
    {
        gameManager = GameManagerScript.Instance;
        gameManager.HUD = gameObject;
        scoreStackTimer = 0;
        AddEventListeners();
        UpdateAmmoWithGun(PlayerAmmoScript.Instance.gun);
    }

    void AddEventListeners()
    {
        EventManagerScript.AddEventListener(AccurateBulletFireEvent.NAME, StartWarning);
        EventManagerScript.AddEventListener(AccurateBulletGoneEvent.NAME, StopWarning);
        EventManagerScript.AddEventListener(BossInvinsibleEvent.NAME,OnBossInvinsible);
        EventManagerScript.AddEventListener(GunUpdateEvent.NAME, UpdateAmmo);
        EventManagerScript.AddEventListener(ArriveWaypointEvent.NAME, CheckForBossFightStart);
        EventManagerScript.AddEventListener(BossHPChangeEvent.NAME, UpdateBossHP);
    }

    // Update is called once per frame
    void Update()
    {
        whiteshotSprite.GetComponent<CanvasGroup>().alpha = 0f;
        if (gameManager.TimerRunning())
            scoreStackTimer = (scoreStackTimer > 0 ? scoreStackTimer - 1 : 0);
        if (scoreStackTimer < 1)
        {
            scoreStackText.GetComponent<CanvasGroup>().alpha = 0;
            scoreStack = 0;
        }

        if (GameManagerScript.gameRunning)
        {
            updateHealth(gameManager.health);
            timeLeftText.text = GameManagerScript.Instance.timeLeft.ToString("F1");
            UpdateWarning();
            if (criticalTimer < 1)
                critical.alpha = 0;
        }
        warnCounter = (warnCounter > 6 ? 0 : warnCounter + 1);
        criticalTimer = (criticalTimer > 0 ? criticalTimer - 1 : 0);
        
        //if (BossTankStatScript.Instance != null)
        //{
        //    if (BossTankStatScript.Instance.activated)
        //    {
        //        this.bossHPBar.gameObject.SetActive(true);
        //        this.bossHPBar.value = BossTankStatScript.Instance.GetHPPercent();
        //    }
        //}
    }

    void UpdateWarning()
    {
        if (currentlyFlyingAccurateBulletCount > 0)
        {
            if (warnCounter % 6 > 2)
            {
                warning.gameObject.SetActive(false);
            }
            else
            {
                warning.gameObject.SetActive(true);
            }
            warnCounter++;
        }
        else
        {
            warning.gameObject.SetActive(false);
            warnCounter = 0;
        }
    }
    void UpdateBossHP(BaseEvent e)
    {
        BossHPChangeEvent _event = e as BossHPChangeEvent;

        //Debug.Log("receive boss hp change event, hp is " + _event.currentHPPercent);
        this.bossHPBar.value = _event.currentHPPercent;
    }
    void CheckForBossFightStart(BaseEvent e)
    {
        ArriveWaypointEvent _event = e as ArriveWaypointEvent;
        if (_event.waypointID.StartsWith("BossFight"))
        {
            //Debug.Log("boss fight starts!!!");
            // Arrive at BossFight
            this.bossHPBar.gameObject.SetActive(true);
        }
    }
    void SetHPBarEnable(bool enable)
    {
        if (enable)
        {
            bossHPFill.color = Color.red;
        } else
        {
            bossHPFill.color = Color.grey;
        }
    }
    public void OnBossInvinsible(BaseEvent e)
    {
        BossInvinsibleEvent _event = e as BossInvinsibleEvent;
        if (_event.isInvinsible)
        {
            SetHPBarEnable(false);
        } else
        {
            SetHPBarEnable(true);
        }
    }
    public void heal()
    {
        gameManager.health = (gameManager.health < 4 ? gameManager.health + 1 : 4);
        AudioManagerScript.Instance.RequestFX(5, GameManagerScript.Instance.Player.transform);
    }
    public void getHit()
    {
        criticalTimer = 15;
        critical.alpha = 1;
    }
    public void ScoreShot(Vector3 mousePosition, bool hit)
    {
        
        whiteshotSprite.GetComponent<CanvasGroup>().alpha = 0.25f;
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(this.UICanvas, mousePosition, HUDCamera, out pos);
        transform.position = this.UICanvas.TransformPoint(pos);
        var gunshot = Instantiate(gunMissSprite) as RectTransform;
        gunshot.SetParent(UICanvas);
        gunshot.position = this.UICanvas.TransformPoint(pos);
        gunshot.localScale = Vector3.one;

        if (hit)
        {
            gunshot.GetComponent<GunshotSpriteScript>().hit = true;
        } else
        {
            gunshot.GetComponent<GunshotSpriteScript>().hit = false;
            return;
        }

        GameManagerScript.Instance.score += 100 + scoreStack * 100;
        scoreText.text = "" + GameManagerScript.Instance.score;
        scoreStack++;
        scoreStackTimer = (int)(1.0f / Time.deltaTime * 3);
        scoreStackText.GetComponent<Text>().text = scoreStack + " hits";
        scoreStackText.GetComponent<CanvasGroup>().alpha = 1;
    }

    public void StartWarning(BaseEvent e)
    {
        currentlyFlyingAccurateBulletCount++;
    }
    public void StopWarning(BaseEvent e)
    {
        currentlyFlyingAccurateBulletCount--;
        if (currentlyFlyingAccurateBulletCount < 0)
        {
            Debug.LogError("currentlyFlyingAccurateBulletCount cannot be less than one. Some lethal bullet was fired without firing event");
        }
    }
   
    void UpdateAmmoWithGun(Gun gun)
    {
        if (gun != PlayerAmmoScript.Instance.gun)
        {
            return;
        }
        int count = gun.clipLeft;
        int maxClip = gun.clipCapacity;

        bulletsLeftText.gameObject.GetComponent<CanvasGroup>().alpha = 1;

        if (gun.infiniteStorage)
        {
            bulletsLeftText.gameObject.SetActive(false);
        }
        else
        {
            bulletsLeftText.gameObject.SetActive(true);
        }

        foreach (Transform bullet in bulletContainer)
        {
            Destroy(bullet.gameObject);
        }
        for (int i = 0; i < count; i++)
        {
            var bull = Instantiate(bullet) as RectTransform;
            bull.SetParent(bulletContainer);
            bull.localPosition = new Vector3(180 / maxClip * i, 0, 0);
            bull.localScale = Vector3.one;
        }
        bulletsLeftText.gameObject.GetComponent<Text>().text = "" + gun.storageLeft;
        gunText.text = gun.gunName;
    }

    void UpdateAmmo(BaseEvent e)
    {
        GunUpdateEvent _event = e as GunUpdateEvent;
        Gun gun = _event.gun;
        UpdateAmmoWithGun(gun);
    }
    void updateHealth(int health)
    {
        int index = 0;
        foreach (Transform ping in healthContainer)
        {
            if (index < health)
            {
                ping.gameObject.SetActive(true);
            }
            else
            {
                ping.gameObject.SetActive(false);
            }
            index++;
        }
    }
}