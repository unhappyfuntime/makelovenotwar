﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

delegate void Phase();
public class BossZeppelinAIScript :Singleton<BossZeppelinAIScript> {

    //private bool started = false;
    private Phase phaseUpdateFunc = null;

    private int enemyKilled = 0;
    private const int phase2RequiredEnemyKills = 12;
    private const int phase4RequiredEnemyKills = 15;

    public GameObject bomb;
    public Transform bombOrigin;
    public Transform BombContainer;

    public float bossHPPercent = 1; // IMPORTANT: This variable is only a convinient storage of boss hp value.
    // Don't use if as the data source. 
    // Use the value in BossZeppelinStatScript as the single source of truth.

    private IEnumerator bombDropingCoroutine;

	void Start () {
        EventManagerScript.AddEventListener(ArriveAtBossStageEvent.NAME, OnArriveAtBoss);
	}
	
	// Update is called once per frame
	void Update () {
        if (phaseUpdateFunc != null)
        {
            phaseUpdateFunc();
        }
	}

    void OnArriveAtBoss(BaseEvent e)
    {
        //Debug.Log("boss event fired");
        //started = true;
        EventManagerScript.AddEventListener(BossHPChangeEvent.NAME, OnBossHPChange);
        EventManagerScript.AddEventListener(BossDieEvent.NAME, this.OnBossDie);
        SetUpPhase1();

    }

    void OnBossHPChange(BaseEvent e)
    {
        BossHPChangeEvent _event = e as BossHPChangeEvent;
        this.bossHPPercent = _event.currentHPPercent;
    }
    // Phase 1: 
    void SetUpPhase1()
    {
        // do set up work
        //addEventListener for when to change to phase2
        BossZeppelinMovementScript.Instance.JumpToID("Phase1_enter");
        phaseUpdateFunc = UpdatePhase1;
        //Debug.Log("Phase 1 started");
    }

    void UpdatePhase1()
    {
        if (bossHPPercent < 0.7)
        {
            //Debug.Log("Move to next one");

            //BossZeppelinMovementScript.Instance.JumpToID("Phase1_leave"); // jump to Phase1_MovingOut
            // clean up. Such as remove eventListeners
            EventManagerScript.FireEvent(ProceedEvent.NAME, new ProceedEvent());
            SetUpPhase2();
        }
    }

    void SetUpPhase2()
    {
        GetComponent<InvinsibleScript>().isInvinsible = true;
        EventManagerScript.FireEvent(BossInvinsibleEvent.NAME, new BossInvinsibleEvent(true));
        phaseUpdateFunc = UpdatePhase2;
        //Debug.Log("Phase 2 started");
        enemyKilled = 0;
        EventManagerScript.AddEventListener(EnemyDieEvent.NAME, IncrementEnemyKilled);

    }
    
    void UpdatePhase2()
    {
        // do stuff
        if (enemyKilled >= phase2RequiredEnemyKills)
        {
            EventManagerScript.FireEvent(ProceedEvent.NAME, new ProceedEvent());
            EventManagerScript.RemoveEventListener(EnemyDieEvent.NAME, IncrementEnemyKilled);
            SetUpPhase3();
        }
    }

    void SetUpPhase3()
    {
        GetComponent<InvinsibleScript>().isInvinsible = false;
        EventManagerScript.FireEvent(BossInvinsibleEvent.NAME, new BossInvinsibleEvent(false));
        //Debug.Log("Phase 3 started");
        phaseUpdateFunc = UpdatePhase3;
        BossZeppelinMovementScript.Instance.JumpToID("Phase3_enter");

        bombDropingCoroutine = BombDecision();
        StartCoroutine(bombDropingCoroutine);
        
    }
    Vector3 GetRandomPointNearBy(Vector3 origin, float radius)
    {
        Vector2 rand = Random.insideUnitCircle* radius ;

        return origin + new Vector3(rand.x, 0, rand.y);
    }
    void DropBomb()
    {
        BombScript newBomb = Instantiate(bomb).GetComponent<BombScript>();
        newBomb.transform.SetParent(BombContainer);
        Vector3 destination = PlayerMovementScript.Instance.transform.position;
        newBomb.Drop(bombOrigin.position, GetRandomPointNearBy(destination, 10.0f));
    }

    IEnumerator BombDecision()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3f, 6f));
            DropBomb();
        }
            
    }

    void UpdatePhase3()
    {
        if (bossHPPercent < 0.4)
        {
            StopCoroutine(bombDropingCoroutine);
            foreach (Transform remainedBomb in BombContainer)
            {
                Destroy(remainedBomb);
            }
            //Debug.Log("Move to next one");
            EventManagerScript.FireEvent(ProceedEvent.NAME, new ProceedEvent());
            // clean up. Such as remove eventListeners
            SetUpPhase4();
            enemyKilled = 0;
            
        }

        
        // boss start droping bombs
        if (Input.GetKeyDown(KeyCode.D))
        {
            DropBomb();
        }
    }


    void SetUpPhase4()
    {
        GetComponent<InvinsibleScript>().isInvinsible = true;
        EventManagerScript.FireEvent(BossInvinsibleEvent.NAME, new BossInvinsibleEvent(true));
        
        phaseUpdateFunc = UpdatePhase4;
        enemyKilled = 0;
        EventManagerScript.AddEventListener(EnemyDieEvent.NAME, IncrementEnemyKilled);
    }

    void UpdatePhase4()
    {
        
        if (enemyKilled >= phase4RequiredEnemyKills)
        {
            
            EventManagerScript.FireEvent(ProceedEvent.NAME, new ProceedEvent());
            EventManagerScript.RemoveEventListener(EnemyDieEvent.NAME, IncrementEnemyKilled);
            SetUpPhase5();
            
        }
    }

    void SetUpPhase5()
    {
        GetComponent<InvinsibleScript>().isInvinsible = false;
        EventManagerScript.FireEvent(BossInvinsibleEvent.NAME, new BossInvinsibleEvent(false));
        //Debug.Log("Phase 5 started");
        BossZeppelinMovementScript.Instance.JumpToID("Phase5_enter");
        phaseUpdateFunc = null;
        
    }

    void IncrementEnemyKilled(BaseEvent e)
    {
        enemyKilled++;

    }

    void OnBossDie(BaseEvent e)
    {
        // kill all enemy
        //Debug.Log("boss die");
        ClearSoldiers();
    }

    void ClearSoldiers()
    {
        enemyContainerScript.Instance.KillAllEnemies();
    }
}
