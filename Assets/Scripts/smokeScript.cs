﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class smokeScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(disappear());
	}

    IEnumerator disappear()
    {
        yield return new WaitForSeconds(1.8f);
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
