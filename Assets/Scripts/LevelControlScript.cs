﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelControlScript : MonoBehaviour {

    public GameObject levelSelect;
    public GameObject titleStart;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void openMain()
    {
        titleStart.GetComponent<CanvasGroup>().alpha = 0;
        titleStart.GetComponent<CanvasGroup>().interactable = false;
        titleStart.GetComponent<CanvasGroup>().blocksRaycasts = false;
        levelSelect.GetComponent<CanvasGroup>().alpha = 1;
        levelSelect.GetComponent<CanvasGroup>().interactable = true;
        levelSelect.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void ToLevel(string name)
    {
        //Debug.Log("loading scene " + name);
        SceneManager.LoadScene(name, LoadSceneMode.Single);
    }
}
