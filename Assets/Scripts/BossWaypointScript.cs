﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossWaypointType { MOVE, MOVE_END};
/**
 * Boss waypoint have two different types
 * Move: when reach this waypoint, slowly moving to next waypoint
 * MoveEnd: when reach this waypoint, stop and wait for instruction.
 *      When notified, change target position immediately to the next waypoint without tweening.
 */
public class BossWaypointScript : MonoBehaviour {
    public float moveSpeed = 5;
    public float turnSpeed = 1.0f / 50.0f;
    public BossWaypointType type = BossWaypointType.MOVE;

    public string waypointID;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Arrive(Transform movingObject)
    {
        movingObject.position = transform.position;
        movingObject.rotation = transform.rotation;
        //if (this.type == BossWaypointType.MOVE_END)
        //{
        //    EventManagerScript.AddEventListener(BossProceedEvent.NAME, OnLeave);
        //}
    }

    //void OnLeave(BaseEvent e)
    //{
    //    EventManagerScript.RemoveEventListener(BossProceedEvent.NAME, OnLeave);
    //}
}
