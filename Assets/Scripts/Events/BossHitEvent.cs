﻿class BossHitEvent: BaseEvent
{
    public const string NAME = "BossGotHit";
    public int damage;
    public BossHitEvent(int damage): base(NAME)
    {
        this.damage = damage;
    }
}
