﻿class BossInvinsibleEvent: BaseEvent
{
    public const string NAME = "BossInvinsible";
    public bool isInvinsible;
    public BossInvinsibleEvent(bool isInvisible) : base(NAME)
    {
        this.isInvinsible = isInvisible;
    }
}
