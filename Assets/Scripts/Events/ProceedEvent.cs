﻿/**
 * Fire this event when player is in hold or BossHold position to make them move to next waypoint.
 */
class ProceedEvent: BaseEvent
{
    public const string NAME = "Proceed";
    public ProceedEvent() : base(NAME) { }
}

