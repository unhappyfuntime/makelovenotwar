﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDieEvent : BaseEvent {
    public static string NAME = "EnemyDie";
    public int enemyType;
    
    public EnemyDieEvent(int enemyType): base(NAME)
    {
        this.enemyType = enemyType;
    }

    
}
