﻿public enum GunUpdateAction {
    EQUIP,
    SHOOT,
    RELOAD,
    SUPPLY
};
class GunUpdateEvent: BaseEvent
{
    public const string NAME = "GunEquip";
    public Gun gun;
    public GunUpdateAction action;
    public GunUpdateEvent(Gun gun, GunUpdateAction action): base(NAME)
    {
        this.gun = gun;
        this.action = action;
    }
}

