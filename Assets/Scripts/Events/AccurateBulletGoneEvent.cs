﻿class AccurateBulletGoneEvent: BaseEvent
{
    public const string NAME = "AccurateBulletGone";

    public AccurateBulletGoneEvent(): base(NAME) { }
}
