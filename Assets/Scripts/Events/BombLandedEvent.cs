﻿class BombLandedEvent: BaseEvent
{
    public const string NAME = "Bomb landed";

    public BombLandedEvent(): base(NAME)
    {

    }
}
