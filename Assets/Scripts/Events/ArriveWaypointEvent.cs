﻿class ArriveWaypointEvent: BaseEvent
{
    public static string NAME = "ArriveWaypoint";
    public string waypointID; // only some important waypoints (such as boss waypoint) have waypointID. 
    public ArriveWaypointEvent(string waypointID): base(NAME)
    {
        this.waypointID = waypointID;
    } 
}

