﻿class BossHPChangeEvent: BaseEvent
{
    public const string NAME = "BossHPChange";
    public float currentHPPercent;
    public BossHPChangeEvent(float currentHPPercent): base(NAME)
    {
        this.currentHPPercent = currentHPPercent;
    }
}

