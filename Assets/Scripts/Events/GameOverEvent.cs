﻿public enum GameOutcome
{
    WIN,
    LOST
}
class GameOverEvent : BaseEvent
{
    public const string NAME = "GameOver";
    public GameOutcome outcome;
    public int score;

    public GameOverEvent(GameOutcome outcome, int score): base(NAME)
    {
        this.outcome = outcome;
        this.score = score;
    }
}

