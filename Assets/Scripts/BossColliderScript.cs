﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossColliderScript : Singleton<BossColliderScript>, ICollider {
    public void OnHit(Collider collider, int damage)
    {
        var invinsibles = GetComponentsInParent<InvinsibleScript>(false);
        var isInvinsible = false;
        foreach (var invisibleScript in invinsibles)
        {
            isInvinsible = isInvinsible || invisibleScript.isInvinsible;
        }
        if (!isInvinsible)
        {
            EventManagerScript.FireEvent(BossHitEvent.NAME, new BossHitEvent(damage));
        }
    }
}
