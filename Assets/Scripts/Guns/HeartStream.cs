﻿using UnityEngine;

class HeartStream:Gun
{
    public override bool PerformShooting(Vector3 screenPosition)
    {
        return ShootSingle(screenPosition);
    }
}

