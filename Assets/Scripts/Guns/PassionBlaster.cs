﻿using UnityEngine;

class PassionBlaster: Gun
{
    public int bulletsPerShot = 5;
    public int blasterRadius = 40;

    public override bool PerformShooting(Vector3 screenPosition)
    {
        Vector3 nearPosition;
        bool hitted = false;

        hitted = hitted || ShootSingle(screenPosition);
        
        for (int i = 0; i < bulletsPerShot-1; i++)
        {
            nearPosition = screenPosition + new Vector3(Random.Range(-blasterRadius, blasterRadius), 
                Random.Range(-blasterRadius, blasterRadius));
            hitted = ShootSingle(nearPosition) || hitted;
        }
        return hitted;
    }

}

