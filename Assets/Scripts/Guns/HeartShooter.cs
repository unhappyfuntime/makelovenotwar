﻿using UnityEngine;
class HeartShooter: Gun
{
    override public bool PerformShooting(Vector3 screenPosition)
    {
        return ShootSingle(screenPosition);
    }
}

