﻿using UnityEngine;


[System.Serializable]
public class Gun
{
    // In this class, "clip" means the container of bullets in the gun.
    // "Storage" means the container that player can take bullets from, in order to fill the clip

    public string gunName;
    public bool isAutomatic;
    public bool infiniteStorage;
    public int storageCapacity; // max capacity of the storage
    public int storageLeft; // current count of bullets in the storage
    public int clipCapacity; // the size of the gun's clip
    public int clipLeft; // bullets currently in the gun
    public int shootSound;

    // reload this gun
    public void Reload()
    {
        //Debug.Log("Reloading");
        Debug.Assert(clipLeft >= 0 && clipLeft <= clipCapacity);
        var needed = clipCapacity - clipLeft;

        if (infiniteStorage)
        {
            clipLeft = clipCapacity;
        }
        else
        {

            int provided = Mathf.Min(storageLeft, needed);
            storageLeft -= provided;
            clipLeft += provided;
        }
        //Debug.Log("gun left " + clipLeft);
        EventManagerScript.FireEvent(GunUpdateEvent.NAME, new GunUpdateEvent(this, GunUpdateAction.RELOAD));
    }
    // Add additional bullets to storage as reward
    public void Supply(int amount)
    {
        storageLeft += Mathf.Min(amount, storageCapacity);
        AudioManagerScript.Instance.RequestFX(6, GameManagerScript.Instance.Player.transform);
    }
    public void Equip()
    {
        EventManagerScript.FireEvent(GunUpdateEvent.NAME, new GunUpdateEvent(this, GunUpdateAction.EQUIP));
        Reload();
    }
    public bool HasBullets
    {
        get
        {
            return clipLeft > 0;
        }
    }

    public bool PopBullet()
    {
        if (HasBullets)
        {
            clipLeft--;
            EventManagerScript.FireEvent(GunUpdateEvent.NAME, new GunUpdateEvent(this, GunUpdateAction.SHOOT));
            AudioManagerScript.Instance.RequestFX(shootSound, GameManagerScript.Instance.Player.transform);
            return true;
        }
        return false;
    }

    public bool ShootSingle(Vector3 screenPosition)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        RaycastHit hit;
        bool hitConfirm = false;
        if (Physics.Raycast(ray, out hit))
        {
            //Debug.Log(hit.collider.gameObject);
            ICollider collider = hit.collider.GetComponent<ICollider>();
            if (collider != null)
            {
                collider.OnHit(hit.collider, Constants.PLAYER_DEBUG_BULLET_DAMAGE);
                hitConfirm = true;
                
            }
        }

        HUDManagerScript.Instance.ScoreShot(screenPosition, hitConfirm);
        return hitConfirm;
    }
    public virtual bool PerformShooting(Vector3 screenPosition)
    {
        return false;
    }
    public bool Shoot(Vector3 mousePosition)
    {
        //if (PlayerMovementScript.Instance.hiding)
        //    return false;
        bool shooted = this.PopBullet();
        if (!shooted)
        {
            return false;
        }

        return PerformShooting(mousePosition);
    }
}