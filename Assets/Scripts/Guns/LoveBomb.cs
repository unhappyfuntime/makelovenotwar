﻿using UnityEngine;

class LoveBomb:Gun
{
    public override bool PerformShooting(Vector3 screenPosition)
    {
        int enemies = enemyContainerScript.Instance.transform.childCount;
        for (int i = 0; i < enemies; i++)
        {
            enemyContainerScript.Instance.transform.GetChild(i).GetComponent<enemyScript>().getHit(20);
        }
        return true;
    }
}

