﻿using UnityEngine;
using System.Collections;

public class PlayerKeyboardEventManager : MonoBehaviour
{
    private bool autoshot = true;
    private PlayerMovementScript camScript;
    private PlayerAmmoScript ammo;
    private const KeyCode NEXT_KEY = KeyCode.Space;
    private const KeyCode HIDE_KEY = KeyCode.LeftShift;

    private const KeyCode RELOAD_KEY = KeyCode.L;
    private const KeyCode SWAP_KEY = KeyCode.S;

    private const KeyCode DEBUG_BOSS_KEY = KeyCode.B;

    void Start()
    {
        camScript = PlayerMovementScript.Instance;
        ammo = PlayerAmmoScript.Instance;
    }

    private IEnumerator AutoshotDelay()
    {
        yield return new WaitForSeconds(0.1f);
        autoshot = true;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(HIDE_KEY) && GameManagerScript.gameRunning)
        {
            camScript.OnHideDown();
            ammo.gun.Reload();
        }
        if (Input.GetKeyUp(HIDE_KEY) && GameManagerScript.gameRunning)
        {
            camScript.OnHideUp();
        }
        if (Input.GetKeyDown(NEXT_KEY))
        {
            camScript.Proceed();
        }

        if (Input.GetKeyDown(SWAP_KEY) && GameManagerScript.gameRunning)
        {
            ammo.SwapGun();
        }

        if (Input.GetKeyDown(DEBUG_BOSS_KEY))
        {
            camScript.DebugToBoss();
        }
        if (Input.GetMouseButtonDown(0) && GameManagerScript.gameRunning)
        {
            if (ammo.gun.isAutomatic)
            {
                return;
            }
            ammo.Shoot(Input.mousePosition);
        }
        if (Input.GetMouseButton(0) && GameManagerScript.gameRunning)
        {
            if (!ammo.gun.isAutomatic || !autoshot)
            {
                return;
            }
            ammo.Shoot(Input.mousePosition);
            autoshot = false;
            StartCoroutine(AutoshotDelay());
        }
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            BossTankAIScript.Instance.Sweep();
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            BossTankAIScript.Instance.StartShooting(false);
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            BossTankAIScript.Instance.StartShooting(true);
        }

        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            BossTankAIScript.Instance.StopShooting();
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            camScript.DebugNextPoint();
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            // kill boss instantly
            if (BossStatScript.SafeCheckExistence())
            {
                BossStatScript.Instance.HP = -1;
                EventManagerScript.FireEvent(BossHPChangeEvent.NAME, new BossHPChangeEvent(BossStatScript.Instance.GetHPPercent()));
            }
            
        }
    }
}
