﻿using UnityEngine;
using System.Collections;

public class AudioSourceScript : MonoBehaviour {

	public void PlayFX(AudioClip a)
    {
        GetComponent<AudioSource>().clip = a;
        GetComponent<AudioSource>().volume = 0.5f;
        GetComponent<AudioSource>().priority = 255;
        StartCoroutine(play());
    }
    public void PlayFX(AudioClip a, float time)
    {
        GetComponent<AudioSource>().clip = a;
        GetComponent<AudioSource>().volume = 0.5f;
        GetComponent<AudioSource>().priority = 255;
        StartCoroutine(play(time));
    }

    public void PlayMusic(AudioClip a)
    {
        GetComponent<AudioSource>().clip = a;
        GetComponent<AudioSource>().loop = true;
        GetComponent<AudioSource>().volume = 0.06f;
        GetComponent<AudioSource>().priority = 0;
        GetComponent<AudioSource>().Play();
    }

    IEnumerator play(float time)
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }

    IEnumerator play()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
        Destroy(gameObject);
    }

}
