﻿using UnityEngine;
using System.Collections;

public class AudioManagerScript : Singleton<AudioManagerScript> {

    public AudioClip[] soundFX;
    public AudioClip[] soundMusic;

    public Transform audioClip;

    Transform music;
    int musicIndex;
    bool stopping;


	// Use this for initialization
	void Start () {
        stopping = false;
	}
	
    public void RequestFX(int id)
    {
        var source = Instantiate(audioClip) as Transform;
        source.SetParent(transform);
        source.GetComponent<AudioSourceScript>().PlayFX(soundFX[id]);
    }
    public void RequestFX(int id, Transform parent)
    {
        var source = Instantiate(audioClip) as Transform;
        source.SetParent(parent);
        source.GetComponent<AudioSourceScript>().PlayFX(soundFX[id]);
    }
    public void RequestFX(int id, float time, Transform parent)
    {
        var source = Instantiate(audioClip) as Transform;
        source.SetParent(parent);
        source.GetComponent<AudioSourceScript>().PlayFX(soundFX[id], time);
    }

    public void RequestMusic(int id)
    {
        if (music != null)
        {
            if (music.gameObject != null && musicIndex == id)
            {
                return;
            }
            else if (music.gameObject != null)
            {
                Destroy(music.gameObject);
            }
        }
        music = Instantiate(audioClip) as Transform;
        music.SetParent(transform);
        musicIndex = id;
        music.GetComponent<AudioSourceScript>().PlayMusic(soundMusic[id]);
    }

    public void StopMusic()
    {
        if (music != null)
        {
            if (music.gameObject != null)
            {
                StartCoroutine(stoppingMusic());
                musicIndex = -1;
            }
        }
    }
    IEnumerator stoppingMusic()
    {
        stopping = true;
        yield return new WaitForSeconds(1f);
        stopping = false;
        Destroy(music.gameObject);
    }

    // Update is called once per frame
    void Update ()
    {
        if (music == null)
            return;
        if (music.gameObject == null)
            return;
        if (stopping)
            music.gameObject.GetComponent<AudioSource>().volume -= Time.fixedDeltaTime * 0.04f;
        else
            music.gameObject.GetComponent<AudioSource>().volume = 0.06f;
    }
}
