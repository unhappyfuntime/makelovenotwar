﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;


[CustomEditor(typeof(WayPointInspector))]
class WayPointDebugInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WayPointInspector inspector = (WayPointInspector)target;
        if (GUILayout.Button("Move player here"))
        {
            GameObject player = GameObject.Find("Player");
            player.transform.position = inspector.transform.position;
            player.transform.rotation = inspector.transform.rotation;
        }
    }
}

#endif