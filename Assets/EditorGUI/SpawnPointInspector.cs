﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPointInspector : MonoBehaviour {


    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        foreach (Transform child in transform)
        {
            Gizmos.DrawLine(transform.position, child.position);
        }
    }
	// Update is called once per frame
	void Update () {

	}
}
