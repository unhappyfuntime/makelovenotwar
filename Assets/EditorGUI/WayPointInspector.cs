﻿using UnityEngine;
using System.Collections;

public class WayPointInspector : MonoBehaviour {

	public void OnDrawGizmos()
    {
        WaypointScript waypoint = GetComponent<WaypointScript>();
        Gizmos.color = Color.blue;
        foreach (enemySpawnerScript spawner in waypoint.enemySpawnPoints)
        {
            Gizmos.DrawLine(transform.position, spawner.transform.position);
        }

    }
}
