### Changes on 10/23 by Yiou  
1. Created Prefab for waypoint
2. For each waypoint, use relative position for hide pos. 
3. Created a container for camera, called Player
4. Player's cameraScript(to be renamed to PlayerScript) now takes a container for hold pos, instead of an array of hold pos.
It will fill the array of hold pos at run time. This is easier for editing waypoint (talk about it below)
5. Created a WaypointDebugCamera for viewing how scene look like at each hold pos.  

To edit hold pos:   

1. click play. 
2. make ajustment for transform and hide postition. You can pressed left shift for see result.
3. On inspector for each waypoint, you can click on "View from waypoint" or "View from hide pos" to use the debug camera. REMEMBER to click "Exit debug view" to return to the main camera
4. When you are done, select everything inside the PlayerPositions **AND** the PlayerPositions object, copy them,
5. Exit play mode. Delete PlayerPositions. Paste the copied PlayerPositions.
6. Drag the newly pasted PlayerPositions to Player's CameraScript.


### Changes on 10/29 by Yiou
1. Refactor the script a bit more.
2. Change `camScript` to `PlayerMovementScript`
3. Added `README.md` including coding style
4. When player is moving between waypoint A and B, waypoint A is the current waypoint. The moveSpeed and turnSpeed are from waypoint A.
5. Added Cheat key `Left Ctrl` and `Right Ctrl` to jump to previous or next waypoint.


### Changes on 12/14 by Yiou
1. Clean up git repo. removed duplicate folders. 
2. Put Soldier mesh inside an empty game object.
3. Added two BoxColliders to bones of soldier, one representing the head, one representing the body (later on we can have different score for example).
4. Added `EnemyTargetScript` to the soldier prefab. This script will dynamically assign `EnemyColliderScript` to the bones at run time(because I don't want to do too much dragging and dropping in editor), and it will get called when child collider is triggered by mouse.
5. Change enemy initial hp to 1, so we can one shot them.
6. when enemy die from one spawn node, it will not respawn.